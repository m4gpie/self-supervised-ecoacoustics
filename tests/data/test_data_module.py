from typing import List

import pytest
import torch  # type: ignore
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt
from click import progressbar

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore

from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.data.data_module import EcoacousticsDataModuleSplitBySite

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("window_seconds", [0.96])
def test_split_data_by_site(root: Path,
                            sample_rate: int,
                            window_seconds: float) -> None:
    # initialize the data
    data_module = EcoacousticsDataModuleSplitBySite(root=str(root),
                                                    sample_rate=sample_rate,
                                                    segment_len=window_seconds * 60,
                                                    target_attrs=['habitat'])
    # fix the times
    data_module.prepare_data()
    # load the dataset
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        download=False,
                        target_attrs=['habitat'])
    # define the proportions
    props = {"val": 0.2, "test": 0.2}
    # get dataset size
    num_observations = len(data)
    # define expected sizes after balancing UK and EC
    section_sizes = {"val": 810, "test": 810, "train": 2430}
    # generate the split subsets
    splits = data_module.split_by_site(data, props=props, seed=0)
    # validate the size of the splits according to the calculated size
    for size, data_split in zip(section_sizes.values(), splits.values()):
        assert size == len(data_split), 'incorrect split size'
    # check there is no overlap between datasets
    val_idx, test_idx, train_idx = [split.s for split in splits.values()]
    assert (val_idx != test_idx).any(), 'val and test datasets overlap'
    assert (test_idx[:, None] != train_idx).any(), 'train and test datasets overlap'
    assert (val_idx[:, None] != train_idx).any(), 'train and val datasets overlap'

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_windows", [60])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_check_null_infinity_values(root: Path,
                                    sample_rate: int,
                                    stft_window_length_seconds: float,
                                    stft_hop_length_seconds: float,
                                    window_seconds: float,
                                    num_windows: int,
                                    num_mel_bins: int) -> None:
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * num_windows,
                        target_attrs=['habitat'])
    # create the spectrogram transform
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_window_length_seconds=stft_window_length_seconds,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              num_mel_bins=num_mel_bins)
    # create the frame transform, non-overlapping frames so window and hop is the same
    frame_transform = Frame(stft_hop_length_seconds=stft_hop_length_seconds,
                            window_seconds=window_seconds,
                            hop_seconds=window_seconds)
    bad_idx = []
    # run through the data and look for null or infinity values
    with progressbar(data) as progress_bar:
        for i, sample in enumerate(progress_bar):
            x = spectrogram_transform.forward(sample.x)
            f = frame_transform.forward(x)
            x_hat = frame_transform.backward(f)
            if (x.isnan().nonzero().any() or
                    f.isnan().nonzero().any() or
                    x_hat.isnan().nonzero().any() or
                    x.isinf().nonzero().any() or
                    f.isinf().nonzero().any() or
                    x_hat.isinf().nonzero().any()):
                bad_idx.append(int(sample.s))
    print("Bad data: ", bad_idx)
    assert len(bad_idx) == 0

