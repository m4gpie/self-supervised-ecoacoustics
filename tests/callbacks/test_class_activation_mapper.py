from typing import Any, Dict, List

import pytest
import yaml
from pathlib import Path
import torch  # type: ignore
import numpy as np
from matplotlib import pyplot as plt

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.callbacks.class_activation_mapper import ClassActivationMapper
from ecoacoustics.types import Stage

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_class_activation_mapper_to_figure(root: Path,
                                           targets: Dict[str, Dict[str, Any]],
                                           sample_rate: int,
                                           stft_hop_length_seconds: float,
                                           stft_window_length_seconds: float,
                                           window_seconds: float,
                                           num_mel_bins: int) -> None:
    # load dataset
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=list(targets.keys()))
    # create transforms
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              stft_window_length_seconds=stft_window_length_seconds,
                                              num_mel_bins=num_mel_bins)
    frame_transform  = Frame(window_seconds=window_seconds,
                             stft_hop_length_seconds=stft_hop_length_seconds,
                             hop_seconds=window_seconds)
    transforms = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # initialize VGGish
    model = torch.hub.load('DavidHurst/torchvggish', 'vggish',
                           preprocess=False,
                           postprocess=False)
    # load a mini-batch of 1 observation
    idxs = torch.randint(len(data), size=(1,))
    # transform to spectrogram, frame then stack together as a batch
    xs = torch.stack([transforms(data.load_sample(idx)) for idx in idxs]).squeeze(1)
    # load target labels and encode as integers
    target_values = data.metadata[list(targets.keys())].iloc[idxs]
    targets_encoded = []
    for target_name, target_attrs in targets.items():
        if target_attrs["type"] == 'categorical':
            targets_encoded.append(target_values[target_name].factorize()[0])
        else:
            targets_encoded.append(np.array(target_values[target_name]))
    # set as floats
    ys = torch.as_tensor(targets_encoded, dtype=float).T
    # stack frames and observations together
    batch = (xs, ys, idxs)
    # create mapper
    class_mapper = ClassActivationMapper(targets=targets,
                                         decoder=data.decoder,
                                         frame_transform=frame_transform,
                                         sample_rate=sample_rate,
                                         stft_hop_length_seconds=stft_hop_length_seconds)
    # run the test
    for fig in class_mapper.to_figures(model, batch):
        plt.show()
        plt.close(fig)
