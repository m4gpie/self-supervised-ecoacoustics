from typing import Any, Dict, List

import pytest
import yaml
from pathlib import Path
import torch  # type: ignore
import numpy as np
from matplotlib import pyplot as plt

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.transforms.min_max_scaler import MinMaxScaler
from ecoacoustics.callbacks.random_forest import RandomForest

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_random_forest(root: Path,
                       targets: Dict[str, Dict[str, Any]],
                       sample_rate: int,
                       stft_hop_length_seconds: float,
                       stft_window_length_seconds: float,
                       window_seconds: float,
                       num_mel_bins: int) -> None:
    target_attributes = targets.keys()
    # load dataset
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=target_attributes)
    # create transforms
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              stft_window_length_seconds=stft_window_length_seconds,
                                              num_mel_bins=num_mel_bins)
    frame_transform  = Frame(window_seconds=window_seconds,
                             stft_hop_length_seconds=stft_hop_length_seconds,
                             hop_seconds=window_seconds)
    transforms = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # remove site from test targets
    test_targets = {target_name: target_attrs
                    for target_name, target_attrs in targets.items()
                    if target_name != 'site'}
    # initialize VGGish
    model = torch.hub.load('DavidHurst/torchvggish', 'vggish',
                           preprocess=False,
                           postprocess=False)
    # create a training and test set
    train_idx = [np.random.randint(len(data)) for i in range(6)]
    test_idx = [np.random.randint(len(data)) for i in range(6)]
    # transform to spectrogram, frame then stack together as a batch
    x_train = torch.stack([transforms(data.load_sample(idx)) for idx in train_idx]).squeeze(1)
    x_test = torch.stack([transforms(data.load_sample(idx)) for idx in test_idx]).squeeze(1)
    # load target labels and encode as integers for categorical data
    train_target_values = data.metadata[target_attributes].iloc[train_idx]
    test_target_values = data.metadata[target_attributes].iloc[test_idx]
    train_targets_encoded = []
    test_targets_encoded = []
    for target_name, target_attrs in targets.items():
        if target_attrs["type"] == 'categorical':
            train_targets_encoded.append(train_target_values[target_name].factorize()[0])
            test_targets_encoded.append(test_target_values[target_name].factorize()[0])
        else:
            train_targets_encoded.append(np.array(train_target_values[target_name]))
            test_targets_encoded.append(np.array(test_target_values[target_name]))
    # set as floats
    y_train = torch.as_tensor(train_targets_encoded, dtype=float).T
    y_test = torch.as_tensor(test_targets_encoded, dtype=float).T
    # stack frames and observations together
    x_train_flat = x_train.view(-1, 1, x_train.size(3), x_train.size(4))
    x_test_flat = x_test.view(-1, 1, x_test.size(3), x_test.size(4))
    # switch to eval
    model.eval()
    with torch.no_grad():
        # do a forward pass on VGGish for the training data
        z_train_flat = model(x_train_flat)
        z_train = z_train_flat.view(x_train.size(0), -1, z_train_flat.size(1))
        z_train_avg = z_train.mean(axis=1)
        # do a forward pass on VGGish for the test data
        z_test_flat = model(x_test_flat)
        z_test = z_test_flat.view(x_test.size(0), -1, z_test_flat.size(1))
        z_test_avg = z_test.mean(axis=1)
        # create callback
        random_forest = RandomForest(targets=test_targets,
                                     decoder=data.decoder)
        # fit to the training data
        forests = random_forest.fit((z_train_avg.numpy(), y_train.numpy()))
        # check number of forests
        assert len(forests) == len(test_targets), 'fits a random forest to each test target'
        # predict the test set
        results = random_forest.predict(forests, (z_test_avg.numpy(), y_test.numpy()))
        # check
        assert type(results["f1_score"]) == float, \
            'returns an average f1 score'

        assert type(results["accuracy"]) == float, \
            'returns an average accuracy score'

        assert type(results["error"]) == float, \
            'returns an average accuracy score'

        for target_name, target_attrs in test_targets.items():
            if target_attrs["type"] == "categorical":
                assert type(results[f"f1_score/{target_name}"]) == np.float64, \
                    f"returns a specific f1 score for {target_name}"
                assert type(results[f"accuracy/{target_name}"]) == np.float64, \
                    f"returns a specific accuracy score for {target_name}"
            elif target_attrs["type"] == "continuous":
                assert type(results[f"error/{target_name}"]) == np.float64, \
                    f"returns a specific error score for {target_name}"
            else:
                raise Exception('target has no valid type')
        # render confusion matrix
        plt.show()
