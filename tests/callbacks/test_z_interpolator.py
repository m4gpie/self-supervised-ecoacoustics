from typing import Any, Dict, List

import pytest
import torch  # type: ignore
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt
from torch.distributions.normal import Normal  # type: ignore

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.models.vae import VAE
from ecoacoustics.models.encoder import Encoder
from ecoacoustics.models.decoder import Decoder
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.callbacks.z_interpolator import ZInterpolator

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("recording_length", [60])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_z_interpolator_interpolate(root: Path,
                                    targets: Dict[str, Dict[str, Any]],
                                    sample_rate: int,
                                    stft_hop_length_seconds: float,
                                    stft_window_length_seconds: float,
                                    window_seconds: float,
                                    recording_length: int,
                                    num_mel_bins: int) -> None:
    target_attributes = targets.keys()
    # create dataset
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=target_attributes)
    # create the spectrogram transform
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              stft_window_length_seconds=stft_window_length_seconds,
                                              num_mel_bins=num_mel_bins)
    frame_transform = Frame(window_seconds=window_seconds,
                            stft_hop_length_seconds=stft_hop_length_seconds,
                            hop_seconds=window_seconds)
    # define a pipeline
    transform = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # load the model
    checkpoint = torch.load(root.parent / 'models' / 'two.ckpt',
                            map_location=torch.device('cpu'))
    # load hparams
    model = VAE.build(**checkpoint["hyper_parameters"])
    # load state dict
    state_dict = dict(checkpoint["state_dict"])
    # hack for an old file
    new_state_dict = {key[6:]: state_dict[key]
                      for key in state_dict.keys() }
    model.load_state_dict(new_state_dict)
    # load 2 samples to interpolate between
    idxs = [0, 2097]
    x = torch.stack([transform(data[idx].x) for idx in idxs]).squeeze(0)
    y = torch.stack([data[idx].y for idx in idxs])
    # initialize callback
    interpolator = ZInterpolator(targets=targets,
                                 decoder=data.decoder,
                                 frame_transform=frame_transform)
    # switch off gradient calculation
    model.eval()
    with torch.no_grad():
        x_flat = x.view(-1, 1, x.size(4), x.size(5))
        # encoder model q_ϕ(z|x) to compute latent vector
        mean_flat, log_variance_flat = model.encoder(x_flat)
        # apply reparameterisation trick to sample z from learned distribution
        mean = mean_flat.view(x.size(0), -1, mean_flat.size(1))
        log_variance = log_variance_flat.view(x.size(0), -1, log_variance_flat.size(1))
        z = model.reparameterise(mean.mean(axis=1),
                                 log_variance.mean(axis=1))
        # check
        fig = interpolator.interpolate(model.decoder, { "x": x, "z": z, "y": y, "s": idxs })
    # show the plot then close the fig
    plt.show()
    plt.close(fig)
