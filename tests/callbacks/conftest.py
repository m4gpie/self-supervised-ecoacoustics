from typing import Any, Dict, List

import pytest
import yaml
from pathlib import Path

@pytest.fixture
def targets(root: Path) -> List[Dict[str, Any]]:
    # load target info
    with open(root.parent / 'config' / 'targets.yml') as file:
        return yaml.load(file, Loader=yaml.Loader)

