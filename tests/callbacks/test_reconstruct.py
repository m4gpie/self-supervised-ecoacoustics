from typing import Dict, Any, List

import pytest
import yaml
import torch  # type: ignore
import numpy as np
import librosa  # type: ignore
from pathlib import Path
from matplotlib import pyplot as plt

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.transforms.min_max_scaler import MinMaxScaler
from ecoacoustics.callbacks.reconstruct import Reconstructor

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_reconstruct_to_figure(root: Path,
                               targets: Dict[str, Dict[str, Any]],
                               sample_rate: int,
                               stft_hop_length_seconds: float,
                               stft_window_length_seconds: float,
                               window_seconds: float,
                               num_mel_bins: int) -> None:
    target_attributes = targets.keys()
    # create dataset
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=target_attributes)
    # create the spectrogram transform
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              stft_window_length_seconds=stft_window_length_seconds,
                                              num_mel_bins=num_mel_bins)
    frame_transform = Frame(window_seconds=window_seconds,
                            stft_hop_length_seconds=stft_hop_length_seconds,
                            hop_seconds=window_seconds)
    # define transform pipeline
    transform = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # load 3 samples
    idxs = torch.randint(len(data), size=(1,))
    xs = torch.stack([transform(data.load_sample(idx)) for idx in idxs]).squeeze(0)
    # load target labels and encode as integers
    target_values = data.metadata[list(targets.keys())].iloc[idxs]
    targets_encoded = []
    for target_name, target_attrs in targets.items():
        if target_attrs["type"] == 'categorical':
            targets_encoded.append(target_values[target_name].factorize()[0])
        else:
            targets_encoded.append(np.array(target_values[target_name]))
    # set as floats
    ys = torch.as_tensor(targets_encoded, dtype=float).T
    # initialize callback
    reconstruct = Reconstructor(targets=targets,
                                frame_transform=frame_transform,
                                decoder=data.decoder)
    # simulate passing model data to the callback, with log var
    figs = reconstruct.to_figures({"x": xs,
                                   "x_hat": xs,
                                   "x_log_var": xs,
                                   "y": ys,
                                   "s": idxs })
    for fig in figs:
        # show the plot then close the fig
        plt.show()
        plt.close(fig)
    # simulate passing model data to the callback, w/o log var
    figs = reconstruct.to_figures({"x": xs,
                                   "x_hat": xs,
                                   "y": ys,
                                   "s": idxs })
    for fig in figs:
        # show the plot then close the fig
        plt.show()
        plt.close(fig)

# @pytest.mark.parametrize("sample_rate", [16_000])
# @pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
# @pytest.mark.parametrize("stft_window_length_seconds", [0.025])
# @pytest.mark.parametrize("window_seconds", [0.96])
# @pytest.mark.parametrize("num_mel_bins", [64])
# def test_reconstruct_to_audio(root: Path,
#                               targets: Dict[str, Dict[str, Any]],
#                               sample_rate: int,
#                               stft_hop_length_seconds: float,
#                               stft_window_length_seconds: float,
#                               window_seconds: float,
#                                num_mel_bins: int) -> None:
#     # TODO
#     target_attributes = targets.keys()
#     data = Ecoacoustics(root=str(root),
#                         sample_rate=sample_rate,
#                         segment_len=window_seconds * 60,
#                         target_attrs=target_attributes)
#     # create the spectrogram transform
#     spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
#                                               stft_hop_length_seconds=stft_hop_length_seconds,
#                                               stft_window_length_seconds=stft_window_length_seconds,
#                                               num_mel_bins=num_mel_bins)
#     frame_transform = Frame(window_seconds=window_seconds,
#                             stft_hop_length_seconds=stft_hop_length_seconds,
#                             hop_seconds=window_seconds)
#     # define transform pipeline
#     transform = torch.nn.Sequential(spectrogram_transform, frame_transform)
#     # load 3 samples
#     idxs = [np.random.randint(len(data)) for i in range(3)]
#     xs = torch.stack([transform(data.load_sample(idx)) for idx in idxs])
#     # initialize callback
#     reconstruct = Reconstructor(targets=targets,
#                                 decoder=data.decoder,
#                                 frame_transform=frame_transform)
#     # simulate passing model data to the callback
#     wav = reconstruct.to_audio({"x": xs,
#                                 "x_hat": xs,
#                                 "x_log_var": xs,
#                                 "s": idxs })
