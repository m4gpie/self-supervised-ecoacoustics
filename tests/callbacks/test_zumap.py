from typing import Any, Dict, List

import pytest
import yaml
from pathlib import Path
import torch  # type: ignore
import numpy as np
from matplotlib import pyplot as plt

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.callbacks.z_umap import ZUMAP

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_zumap_to_figure(root: Path,
                         targets: Dict[str, Dict[str, Any]],
                         sample_rate: int,
                         stft_hop_length_seconds: float,
                         stft_window_length_seconds: float,
                         window_seconds: float,
                         num_mel_bins: int) -> None:
    target_attributes = targets.keys()
    # load dataset
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=target_attributes)
    # create transforms
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                                       stft_hop_length_seconds=stft_hop_length_seconds,
                                                       stft_window_length_seconds=stft_window_length_seconds,
                                                       num_mel_bins=num_mel_bins)
    frame_transform  = Frame(window_seconds=window_seconds,
                                           stft_hop_length_seconds=stft_hop_length_seconds,
                                           hop_seconds=window_seconds)
    transforms = torch.nn.Sequential(spectrogram_transform, frame_transform) # initialize VGGish
    # remove site from test targets
    test_targets = {target_name: target_attrs
                    for target_name, target_attrs in targets.items()
                    if target_name != 'site'}
    model = torch.hub.load('DavidHurst/torchvggish', 'vggish',
                           preprocess=False,
                           postprocess=False)
    # load a mini-batch of observations
    idxs = np.array([np.random.randint(len(data)) for i in range(50)])
    x = torch.stack([transforms(data[idx].x) for idx in idxs]).squeeze(0)
    y = torch.stack([data[idx].y for idx in idxs])
    # switch to eval
    model.eval()
    with torch.no_grad():
        # stack frames and observations together
        x_flat = x.view(-1, 1, x.size(4), x.size(5))
        # do a forward pass on VGGish for our mini-batch, compute some latent representations
        z_flat = model(x_flat)
        z = z_flat.view(x.size(0), -1, z_flat.size(1))
        z_avg = z.mean(axis=1)
        # initialize umap callback module
        z_umap = ZUMAP(targets=test_targets,
                       decoder=data.decoder,
                       save_path=(root.parent / 'tests' / 'fixtures'))
        # convert to numpy and run test
        list(z_umap.to_figures(z_avg.numpy(),
                               y.numpy(),
                               idxs))
        plt.show()
