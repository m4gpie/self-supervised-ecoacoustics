import pytest
import torch
import torchaudio
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore

from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.lib.sketch import plot_mel_spectrogram, plot_waveform

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_windows", [60])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_log_mel_spectrogram_transform(root: Path,
                                       sample_rate: int,
                                       stft_window_length_seconds: float,
                                       stft_hop_length_seconds: float,
                                       window_seconds: float,
                                       num_windows: int,
                                       num_mel_bins: int) -> None:
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=['habitat'])
    # create the spectrogram transform
    transform = LogMelSpectrogram(sample_rate=sample_rate,
                                  stft_window_length_seconds=stft_window_length_seconds,
                                  stft_hop_length_seconds=stft_hop_length_seconds,
                                  num_mel_bins=num_mel_bins)
    # load a single sample
    x = data.load_sample(np.random.randint(len(data)))
    # construct a spectrogram
    S = transform.forward(x)
    # recompose original signal using inverse FFT
    x_hat = transform.backward(S)
    # construct plots for visual comparison
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    # plot signal time-series
    plot_waveform(ax=ax1,
                  data=x.squeeze(0).numpy(),
                  sample_rate=sample_rate,
                  title=r"Input Signal $x$")
    # plot mel
    _, mesh = plot_mel_spectrogram(ax=ax2,
                                   data=S.exp().numpy(),
                                   sample_rate=sample_rate,
                                   cmap="viridis")
    plt.colorbar(mesh,
                 format='%+3.1f dB',
                 ax=ax2,
                 orientation="vertical")
    # plot timeseries of inverse signal
    plot_waveform(ax=ax3,
                  data=x_hat.squeeze(0).numpy(),
                  sample_rate=sample_rate,
                  title=r"Output Signal $\hat{x}$")
    plt.show()
