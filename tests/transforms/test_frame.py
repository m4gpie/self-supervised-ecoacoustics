import pytest
import torch
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore

from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.lib.sketch import plot_mel_spectrogram, plot_waveform

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("hop_seconds", [0.96])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_frame_transform(root: Path,
                         sample_rate: int,
                         stft_hop_length_seconds: float,
                         window_seconds: float,
                         hop_seconds: float,
                         num_mel_bins: int) -> None:
    # initialize the data
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        download=False,
                        target_attrs=['habitat'])
    # create the spectrogram transform
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              num_mel_bins=num_mel_bins)
    # create the frame transform, non-overlapping frames so window and hop is the same
    frame_transform = Frame(stft_hop_length_seconds=stft_hop_length_seconds,
                            window_seconds=window_seconds,
                            hop_seconds=window_seconds)
    # load a single sample
    x = data.load_sample(np.random.randint(len(data)))
    # compute the spectrogram
    spectrogram = spectrogram_transform.forward(x)
    # calculate expected spectrogram values
    num_hops = round(window_seconds / stft_hop_length_seconds)
    num_samples = spectrogram.shape[1]
    # calculate the expected number of frames
    features_sample_rate = 1 / stft_hop_length_seconds
    window_length = int(round(window_seconds * features_sample_rate))
    hop_length = int(round(hop_seconds * features_sample_rate))
    num_frames = 1 + ((num_samples - window_length) // hop_length)
    # reframe the spectrogram
    framed = frame_transform.forward(spectrogram)

    assert framed.shape == torch.Size([1, num_frames, 1, num_hops, num_mel_bins]), \
        'incorrectly framed'

    unframed = frame_transform.backward(framed)

    assert spectrogram.shape == unframed.shape, \
        'incorrectly unframed, shape doesnt match'

    assert (spectrogram == unframed).all(), \
        'incorrectly unframed, values dont match'
