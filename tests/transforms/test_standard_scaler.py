import pytest
import torch
import numpy as np
from pathlib import Path

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.standard_scaler import StandardScaler

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("window_seconds", [0.96])
def test_standard_scaler(root: Path,
                         sample_rate: int,
                         window_seconds: float) -> None:
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=['habitat'])
    # create the spectrogram transform using default params
    transform = LogMelSpectrogram(sample_rate=sample_rate)
    # load n samples, stack as a batch
    xs = torch.stack([transform(data.load_sample(0)) for i in range(10)])
    # create our standard scaler
    scale = StandardScaler(mean=xs.mean(axis=[0, 1], keepdim=True),
                           std=xs.std(axis=[0, 1], keepdim=True))
    # subtract mean and divide by std dev along frequency axis
    ys = scale.backward(scale.forward(xs))
    # check to see if input and output are correctly scaled and descaled equal within a degree of error
    assert (abs(ys - xs) < 1e-6).all(), 'reverse transform invalid'
