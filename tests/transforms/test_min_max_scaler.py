import pytest
import torch
import numpy as np
from pathlib import Path

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.min_max_scaler import MinMaxScaler

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_min_max_scaler(root: Path,
                        sample_rate: int,
                        window_seconds: float,
                        stft_hop_length_seconds: float,
                        num_mel_bins: int) -> None:
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * 60,
                        target_attrs=['habitat'])
    # create the spectrogram transform
    transform = LogMelSpectrogram(sample_rate=sample_rate,
                                  stft_hop_length_seconds=stft_hop_length_seconds,
                                  num_mel_bins=num_mel_bins)
    # load n samples
    xs = torch.stack([transform(data.load_sample(np.random.randint(len(data))))
                      for i in range(10)])
    # create our scaler
    scale = MinMaxScaler(x_max=xs.max(), x_min=xs.min())
    # scale and descale the data
    ys = scale.backward(scale.forward(xs))
    # check to see if input and output are correctly scaled and descaled equal within a degree of error
    assert (abs(xs - ys) < 1e-6).all(), 'reverse transform invalid'

