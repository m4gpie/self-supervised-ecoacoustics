import pytest
import torch # type: ignore
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt
from torch.functional import F  # type: ignore
from click import progressbar
from matplotlib import pyplot as plt
from matplotlib import gridspec as gs  # type: ignore
import librosa  # type: ignore
from librosa import display as libd
from pl_bolts.models.autoencoders.components import resnet18_decoder, resnet18_encoder

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.transforms.min_max_scaler import MinMaxScaler
from ecoacoustics.models.encoder import Encoder
from ecoacoustics.models.decoder import Decoder

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_windows", [60])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_autoencoder_features(root: Path,
                              sample_rate: int,
                              stft_window_length_seconds: float,
                              stft_hop_length_seconds: float,
                              window_seconds: float,
                              num_windows: int,
                              num_mel_bins: int) -> None:
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * num_windows,
                        target_attrs=['habitat'])
    # create the spectrogram transform
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_window_length_seconds=stft_window_length_seconds,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              num_mel_bins=num_mel_bins)
    # create the frame transform, non-overlapping frames so window and hop is the same
    frame_transform = Frame(stft_hop_length_seconds=stft_hop_length_seconds,
                            window_seconds=window_seconds,
                            hop_seconds=window_seconds)
    # generate librosa specgram params
    specgram_params = {"sr": sample_rate,
                       "hop_length": spectrogram_transform.hop_length,
                       "n_fft": spectrogram_transform.fft_length,
                       "y_axis": "mel",
                       "x_axis": "time",
                       "fmax": spectrogram_transform.mel_max_hz,
                       "fmin": spectrogram_transform.mel_min_hz,
                       "cmap": 'viridis' }
    # load a single sample
    sample = data.load_sample(0)
    # compute the spectrogram
    spectrogram = spectrogram_transform.forward(sample)
    framed = frame_transform(spectrogram).squeeze(0)
    # apply normalisation
    scaler = MinMaxScaler(x_min=framed.max(), x_max=framed.min())
    x = scaler.forward(framed)
    # create autoencoder
    pretrain = True
    encoder = Encoder(out_features=128, vggish_pretrain=True)
    decoder = Decoder(in_features=128,
                      out_channels=1,
                      negative_slope=0.03)
    model = torch.nn.ModuleList([encoder, decoder])
    # use adam optimiser
    learning_rate = 5e-4
    optimizer = torch.optim.AdamW(model.parameters(), lr=learning_rate)
    num_steps = 1800
    step_size = 150
    num_cols = 3
    row_offset = 1
    num_rows = int(np.ceil(num_steps / step_size / num_cols)) + row_offset
    losses = np.empty(num_steps)
    fig = plt.figure(figsize=(11.69, 8.27), dpi=100)
    grid_spec = gs.GridSpec(num_rows, num_cols,
                            wspace=0.4,
                            hspace=0.8)
    ax = fig.add_subplot(grid_spec[0, 0])
    # render the original input
    mesh = libd.specshow(librosa.amplitude_to_db(framed[0][0].numpy()),
                         vmin=-100, vmax=-40,
                         ax=ax, **specgram_params)
    plt.colorbar(mesh, format='%+3.1f dB', ax=ax)  # type: ignore
    ax.set_title("Input mel Spectrogram first 0.96s frame", fontsize='medium')  # type: ignore
    # setup using GPU
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    model.to(device)
    x = x.to(device)
    # train
    model.train()
    with progressbar(range(num_steps)) as progress_bar:
        for i, step in enumerate(progress_bar):
            x_hat = decoder.features(encoder.vggish.features(x))
            loss = F.mse_loss(x_hat, x, reduction="none")
            loss = loss.view(loss.size(0), -1).mean(axis=1).mean(axis=0)
            losses[i] = loss.item()
            if (step + 1) % step_size == 0:
                row = int(step / (step_size * num_cols)) + row_offset
                col = int(step / step_size) % num_cols
                ax = fig.add_subplot(grid_spec[row, col])
                # render each reconstruction
                reconstruction = scaler.backward(x_hat[0][0].detach().cpu())
                mesh = libd.specshow(librosa.amplitude_to_db(reconstruction.numpy()),
                                     vmin=-100, vmax=-40,
                                     ax=ax, **specgram_params)
                plt.colorbar(mesh, format='%+3.1f dB', ax=ax)  # type: ignore
                ax.set_title(f"Reconstruction at step {step + 1}", fontsize='medium')  # type: ignore
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
    # plot the loss
    ax = fig.add_subplot(grid_spec[0, -1])
    ax.plot(np.arange(num_steps), losses)
    final_loss = losses[-1].round(decimals=6)
    min_i = losses.argmin()
    min_loss = losses[min_i].round(decimals=6)
    fig.suptitle('Autoencoder learned features on full spectrogram\n'
                 'Displaying the first frame only\n'
                 # f'VGGish Encoder Pretrained: {str(pretrain)}\n'
                 f'Learning Rate: {learning_rate}\n'
                 f'Max Amplitude: {spectrogram.max().round(decimals=4)}\n'
                 f'Min Amplitude: {spectrogram.min().round(decimals=4)}\n'
                 f'Final loss: {final_loss}\n'
                 f'Min Loss: {min_loss} at step {min_i}',
                 fontsize='medium')  # type: ignore
    # save the figure
    fig.tight_layout()
    fig.savefig('./test_autoencoder_features.png')
