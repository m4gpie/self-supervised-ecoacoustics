import pytest
import torch # type: ignore
import numpy as np
import pytorch_lightning as pl  # type: ignore
from pathlib import Path
from matplotlib import pyplot as plt
from torch.functional import F  # type: ignore
from click import progressbar
from matplotlib import pyplot as plt
from matplotlib import gridspec as gs  # type: ignore
import librosa  # type: ignore
from librosa import display as libd

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.transforms.min_max_scaler import MinMaxScaler
from ecoacoustics.models.encoder import Encoder
from ecoacoustics.models.decoder import Decoder
from ecoacoustics.models.vae import VAE

@pytest.mark.parametrize("sample_rate", [16_000])
@pytest.mark.parametrize("stft_window_length_seconds", [0.025])
@pytest.mark.parametrize("stft_hop_length_seconds", [0.010])
@pytest.mark.parametrize("window_seconds", [0.96])
@pytest.mark.parametrize("num_windows", [60])
@pytest.mark.parametrize("num_mel_bins", [64])
def test_vae(root: Path,
             sample_rate: int,
             stft_window_length_seconds: float,
             stft_hop_length_seconds: float,
             window_seconds: float,
             num_windows: int,
             num_mel_bins: int) -> None:
    data = Ecoacoustics(root=str(root),
                        sample_rate=sample_rate,
                        segment_len=window_seconds * num_windows,
                        target_attrs=['habitat'])
    # create the spectrogram transform
    spectrogram_transform = LogMelSpectrogram(sample_rate=sample_rate,
                                              stft_window_length_seconds=stft_window_length_seconds,
                                              stft_hop_length_seconds=stft_hop_length_seconds,
                                              num_mel_bins=num_mel_bins)
    # create the frame transform, non-overlapping frames so window and hop is the same
    frame_transform = Frame(stft_hop_length_seconds=stft_hop_length_seconds,
                            window_seconds=window_seconds,
                            hop_seconds=window_seconds)
    # generate librosa specgram params
    specgram_params = {"sr": sample_rate,
                       "hop_length": spectrogram_transform.hop_length,
                       "n_fft": spectrogram_transform.fft_length,
                       "y_axis": "mel",
                       "x_axis": "time",
                       "fmax": spectrogram_transform.mel_max_hz,
                       "fmin": spectrogram_transform.mel_min_hz,
                       "cmap": 'viridis' }
    # load n samples
    n = 16
    spectrograms = []
    framings = []
    for i in range(n):
        # compute the spectrogram
        sample = data.load_sample(np.random.randint(len(data)))
        spectrogram = spectrogram_transform.forward(sample)
        frames = frame_transform.forward(spectrogram).squeeze(0)
        spectrograms.append(spectrogram)
        framings.append(frames)
    spectrograms = torch.stack(spectrograms)
    x = torch.stack(framings)
    # create VAE
    pretrain = True
    encoder = Encoder(out_features=128, vggish_pretrain=pretrain)
    decoder = Decoder(in_features=128, out_channels=2, negative_slope=0.03)
    model = VAE(encoder, decoder)
    # use adam optimiser
    learning_rate = 1e-4
    optimizer = torch.optim.AdamW(model.parameters(), lr=learning_rate)
    # training params
    num_steps = 4000
    step_size = 500
    num_cols = 2
    row_offset = 1
    num_rows = int(np.ceil(num_steps / step_size / num_cols)) + row_offset
    # collect stats
    losses = np.empty(num_steps)
    kls = np.empty(num_steps)
    nlls = np.empty(num_steps)
    # make plots
    fig_1 = plt.figure(figsize=(11.69, 8.27), dpi=100)
    grid_spec_1 = gs.GridSpec(num_rows, num_cols,
                            wspace=0.4,
                            hspace=0.8)
    ax_1 = fig_1.add_subplot(grid_spec_1[0, 0])
    fig_2 = plt.figure(figsize=(11.69, 8.27), dpi=100)
    grid_spec_2 = gs.GridSpec(num_rows, num_cols,
                            wspace=0.4,
                            hspace=0.8)
    ax_2 = fig_2.add_subplot(grid_spec_2[0, 0])
    # render the original input
    spectrogram = frame_transform.backward(x[0])
    frame = librosa.amplitude_to_db(spectrogram.exp().numpy())
    db_min, db_max = frame.min(), frame.max()
    mesh = libd.specshow(frame,
                         vmin=db_min, vmax=db_max,
                         ax=ax_1, **specgram_params)
    plt.colorbar(mesh, format='%+3.1f dB', ax=ax_1)  # type: ignore
    ax_1.set_title("Input mel Spectrogram", fontsize='small')  # type: ignore
    mesh = libd.specshow(frame,
                         vmin=db_min, vmax=db_max,
                         ax=ax_2, **specgram_params)
    plt.colorbar(mesh, format='%+3.1f dB', ax=ax_2)  # type: ignore
    ax_2.set_title("Input mel Spectrogram", fontsize='small')  # type: ignore
    # setup using GPU
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    model.to(device)
    x = x.to(device)
    # train the model
    model.train()
    with progressbar(range(num_steps)) as progress_bar:
        for i, step in enumerate(progress_bar):
            outputs = model(x)
            loss = outputs["loss"]
            # store losses for plotting
            losses[i] = loss.item()
            nlls[i] = outputs["log_likelihood"].item()
            kls[i] = outputs["kl_divergence"].item()
            # render reconstruction and uncertainty
            if (step + 1) % step_size == 0:
                # calculate correct column
                row = int(step / (step_size * num_cols)) + row_offset
                col = int(step / step_size) % num_cols
                # render reconstruction
                x_hat = outputs["x_hat"]
                ax_1 = fig_1.add_subplot(grid_spec_1[row, col])
                # extract the first reconstruction
                x_hat = x_hat[0].detach().cpu()
                x_hat = frame_transform.backward(x_hat)
                x_hat = x_hat.exp()
                mesh = libd.specshow(librosa.amplitude_to_db(x_hat.numpy()),
                                     vmin=db_min, vmax=db_max,
                                     ax=ax_1, **specgram_params)
                plt.colorbar(mesh, format='%+3.1f dB', ax=ax_1)  # type: ignore
                ax_1.set_title(f"Reconstruction at step {step + 1}", fontsize='small')  # type: ignore
                # calculate std to render uncertainty
                x_log_var = outputs["x_log_var"]
                x_std = (0.5 * x_log_var).exp()
                ax_2 = fig_2.add_subplot(grid_spec_2[row, col])
                x_std = x_std[0].detach().cpu()
                x_std = frame_transform.backward(x_std)
                x_std = x_std.exp()
                mesh = libd.specshow(librosa.amplitude_to_db(x_std.numpy()),
                                     ax=ax_2, **specgram_params)
                plt.colorbar(mesh, format='%+3.1f dB', ax=ax_2)  # type: ignore
                ax_2.set_title(f"Uncertainty at step {step + 1}", fontsize='small')  # type: ignore
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
    # plot the loss
    fig_3 = plt.figure(figsize=(11.69, 8.27), dpi=100)
    ax = fig_3.add_subplot(131)
    ax.plot(np.arange(num_steps), losses)
    ax.set_title("Loss")
    ax = fig_3.add_subplot(132)
    ax.plot(np.arange(num_steps), nlls)
    ax.set_title("Gaussian Log Probability")
    ax = fig_3.add_subplot(133)
    ax.plot(np.arange(num_steps), kls)
    ax.set_title("KL Divergence")
    min_i = losses.argmin()
    min_loss = losses[min_i]
    for i, fig in enumerate([fig_1, fig_2, fig_3]):
        fig.suptitle(f'VAE trained on {n} spectrograms\n'
                     f'Learning Rate: {learning_rate}\n',
                     fontsize='medium')  # type: ignore
        # save the figure
        fig.tight_layout()
        fig.savefig(f'./test_vae_{i}.png')

