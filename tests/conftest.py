import pytest
from pathlib import Path

@pytest.fixture
def root() -> Path:
    return Path().resolve() / 'data'
