from typing import Union, Tuple, List
from numpy import typing as npt

import librosa  # type: ignore
import numpy as np
from librosa import display as libd
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.collections import QuadMesh
from matplotlib.colors import Colormap

def plot_mel_spectrogram(ax: Axes,
                         data: npt.NDArray,
                         sample_rate: int = 16_000,
                         stft_window_length_seconds: float = 0.025,
                         stft_hop_length_seconds: float = 0.010,
                         mel_min_hertz: float = 125,
                         mel_max_hertz: float = 7500,
                         title: str = "Mel Spectrogram",
                         cmap: Union[str, Colormap] = "viridis",
                         **kwargs) -> Tuple[Axes, QuadMesh]:
    window_length = round(sample_rate * stft_window_length_seconds)
    hop_length = round(sample_rate * stft_hop_length_seconds)
    fft_length = 2 ** int(np.ceil(np.log(window_length) / np.log(2.0)))
    mel_db = librosa.amplitude_to_db(data, ref=np.max)
    mesh = libd.specshow(mel_db,
                         sr=sample_rate,
                         hop_length=hop_length,
                         n_fft=fft_length,
                         y_axis="mel",
                         x_axis="time",
                         fmax=mel_max_hertz,
                         fmin=mel_min_hertz,
                         cmap=cmap,
                         ax=ax,
                         **kwargs)
    ax.set_title(title)
    return ax, mesh

def plot_waveform(ax: Axes,
                  data: npt.NDArray,
                  sample_rate: int = 16_000,
                  title: str = r"Signal $x$") -> Axes:
    duration = data.shape[0] / sample_rate
    ts = np.arange(0, duration, 1 / sample_rate)
    ax.plot(ts, data)
    ax.set_title(title)
    ax.margins(x=0)
    return ax

def plot_confusion_matrix(ax: Axes,
                          data: npt.NDArray,
                          class_names: List[str]) -> Axes:
    num_classes = len(class_names)
    # plot the matrix
    ax.matshow(data)
    # assign ticks
    ax.set_xticks(np.arange(num_classes))
    ax.set_yticks(np.arange(num_classes))
    # assign labels
    ax.set_xticklabels(class_names)
    ax.set_yticklabels(class_names)
    # rotate labels by 45 degreees
    plt.setp(ax.get_xticklabels(),
             rotation=45,
             ha="left",
             rotation_mode="anchor")
    plt.setp(ax.get_yticklabels(),
             rotation=45,
             ha="right",
             rotation_mode="anchor")
    return ax


def make_ax_invisible(ax: Axes):
    # lazy load matplotlib
    from matplotlib import ticker
    # set background to white
    ax.set_facecolor('white')
    # make spines invisible
    [ax.spines[side].set_visible(False) for side in ax.spines]
    # set grid to white
    ax.grid(which='major', color='white', linewidth=1.2)
    ax.grid(which='minor', color='white', linewidth=0.6)
    ax.minorticks_on()
    ax.tick_params(which='minor', bottom=False, left=False)
    ax.xaxis.set_minor_locator(ticker.AutoMinorLocator(2))
    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(2))
    # remove all ticks
    ax.set_yticks([])
    ax.set_xticks([])
