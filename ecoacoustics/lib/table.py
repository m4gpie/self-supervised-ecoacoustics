import yaml
import pandas as pd
from matplotlib import pyplot as plt

with open('./config/train_params.yml') as file:
    params = yaml.load(file, Loader=yaml.Loader)

del params["development"]
del params["production"]
del params["deterministic"]
del params["vggish_pretrain"]
del params["vggish_frozen"]

row_names = [str(value) for value in params.keys()]
column = [[str(value)] for value in params.values()]
df = pd.DataFrame([params.values()], columns=params.keys())
df.T.to_csv('./docs/assets/parameters.csv')

def parameter_table(row_names, column):
    fig = plt.figure(figsize=(11.69, 8.27), dpi=80)
    ax = fig.add_subplot(111)
    table = ax.table(cellText=column,
                     colLabels=["Parameter"],
                     rowLabels=row_names,
                     loc="center",
                     edges='closed')
    table.auto_set_column_width(col=list(range(len(column))))
    table.set_fontsize(14)
    table.scale(1.75, 1.75)
    ax.axis('off')
    return fig

fig = parameter_table(row_names, column)
