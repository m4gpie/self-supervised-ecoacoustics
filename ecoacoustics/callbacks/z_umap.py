from typing import Callable, List, Dict, Tuple, Union, Any, Iterator
from numpy import typing as npt

import re
import umap  # type: ignore
import torch  # type: ignore
import wandb  # type: ignore
import numpy as np
import pandas as pd
import pytorch_lightning as pl  # type: ignore
from pathlib import Path
from matplotlib import pyplot as plt
from matplotlib import gridspec as gs  # type: ignore
from matplotlib.figure import Figure  # type: ignore
from matplotlib.patches import Patch  # type: ignore
from matplotlib import colors  # type: ignore
from matplotlib import cm
from conduit.data.datasets.audio.ecoacoustics import SoundscapeAttr  # type: ignore
from ecoacoustics.types import Stage
from conduit.data import TernarySample  # type: ignore
from ecoacoustics.utils import TargetsMixin

Cache = Dict[Stage, Dict[str, List[npt.NDArray]]]

class ZUMAP(TargetsMixin, pl.Callback):
    def __init__(self,
                 targets: Dict[str, Dict[str, Union[str, int]]],
                 decoder: Dict[str, Any],
                 save_path: Union[Path, None] = None,
                 metric: str = 'euclidean',
                 min_distance: int = 0,
                 num_neighbours: int = 50,
                 seed: int = 0,
                 num_components: int = 2,
                 model_name: str = 'VAE Random Forest Classifier',
                 cmap: colors.Colormap = 'rainbow') -> None:
        self.targets = targets
        super().__init__()
        self.decoder = decoder
        self.save_path = save_path
        self.metric = metric
        self.min_distance = min_distance
        self.num_neighbours = num_neighbours
        self.seed = seed
        self.num_components = num_components
        self.model_name = model_name
        self.cmap = cmap
        self.reset_cache()

    def on_predict_batch_end(self,
                             trainer: pl.Trainer,
                             pl_module: pl.LightningModule,
                             output: Dict[Stage, Dict[str, torch.Tensor]],
                             batch: TernarySample,
                             batch_idx: int,
                             dataloader_idx: int) -> None:
        """
        lightning doesnt allow callbacks to receive outputs in on_validation_epoch_end,
        so we'll cache the results of each batch as we go inside the callback as recommended
        in their documentation
        """
        self.cache_data(output[Stage.test])

    def on_predict_epoch_end(self,
                             trainer: pl.Trainer,
                             pl_module: pl.LightningModule,
                             outputs: List[List[Dict[Stage, Dict[str, torch.Tensor]]]]) -> None:
        # run UMAP for each target
        idx = np.stack(self.cache["s"])
        idx, idx_mask = np.unique(idx, return_index=True)
        zs = np.stack(self.cache["z"])[idx_mask]
        ys = np.stack(self.cache["y"])[idx_mask]
        # hack to ensure only z's from unique observations are used,
        # because our combined dataloader is running through all training observations
        # and keeps loading test data, rather than stopping
        print(f"predicting {len(zs)} z's")
        for fig in self.to_figures(zs, ys, idx):
            pl_module.logger.experiment.log({ 'predict/umap': wandb.Image(fig) })
        plt.close(fig)
        self.reset_cache()

    def to_figures(self,
                   zs: npt.NDArray,
                   ys: npt.NDArray,
                   idx: npt.NDArray) -> Figure:
        fig_1 = plt.figure(figsize=(11.69, 8.27), dpi=100)
        fig_2 = plt.figure(figsize=(11.69, 8.27), dpi=100)
        grid_spec = gs.GridSpec(2, 2)
        # use umap parameters from sethi paper
        mapper = umap.UMAP(metric=self.metric,
                           min_dist=self.min_distance,
                           n_neighbors=self.num_neighbours,
                           random_state=self.seed,
                           n_components=self.num_components)
        # fit umap with latent variables
        embedding = mapper.fit_transform(zs)
        # there are 6 targets
        for i, (target_name, target_attrs) in enumerate(self.targets.items()):
            ax_1 = fig_1.add_subplot(grid_spec[int(i / 2), i % 2])
            ax_2 = fig_2.add_subplot(grid_spec[int(i / 2), i % 2])
            # extract the index and name of the target
            target_i, target_type, target_full_name = (int(target_attrs["index"]),
                                                       str(target_attrs["type"]),
                                                       str(target_attrs["full_name"]))
            # create a mask to remove data points with null target values (time)
            mask = np.isnan(ys[:, target_i])
            # select the relevant column
            y_true = ys[~mask, target_i]
            # plot categorical data as discrete points
            if target_type == 'categorical':
                # extract the string label for each data point,
                # y values will be floats, so cast as integer
                labels = np.array([self.decoder[target_name][int(y)]
                                   for y in y_true], dtype='<U3')
                unique_labels = np.unique(labels)
                unique_labels.sort()
                # create a colormap
                colour_key = cm.get_cmap(self.cmap)(np.linspace(0, 1, len(unique_labels)))  # type: ignore
                # make it discrete per label
                label_colour_mapping = {k: colors.to_hex(colour_key[i])
                                        for i, k in enumerate(unique_labels)}
                colours = [str(label_colour_mapping[label])
                           for i, label in enumerate(labels)]
                # generate legend labels
                legend_elements = [Patch(facecolor=colour_key[i], label=unique_labels[i])
                                   for i, _ in enumerate(unique_labels)]
                # plot points
                for ax in [ax_1, ax_2]:
                    ax.scatter(embedding[:, 0], embedding[:, 1],
                               marker='.',
                               label=labels[~mask],
                               c=colours)  # type: ignore
                    # move legend to top left
                    ax.legend(handles=legend_elements,
                              bbox_to_anchor=(0, 1),
                              loc="upper right")
            # plot continuous data using a colormap
            elif target_type == 'continuous':
                if not mask.all():
                    # create a scalar colour map for values
                    norm = colors.Normalize(y_true.min(), y_true.max())
                    scalar_map = cm.ScalarMappable(norm=norm, cmap=self.cmap)  # type: ignore
                    # plot points between max and min and color by value
                    for fig, ax in [(fig_1, ax_1), (fig_2, ax_2)]:
                        ax.scatter(embedding[~mask, 0], embedding[~mask, 1],
                                   c=y_true,
                                   vmin=y_true.min(),
                                   vmax=y_true.max(),
                                   marker='.',
                                   cmap=self.cmap)
                        # create a colorbar
                        fig.colorbar(scalar_map, ax=ax)  # type: ignore
            # remove all axis ticks on main plot
            ax_1.get_yaxis().set_visible(False)  # type: ignore
            ax_1.get_xaxis().set_visible(False)  # type: ignore
            ax_1.set_xticks([])
            ax_1.set_yticks([])
            for ax in [ax_1, ax_2]:
                # set the title
                ax.set_title(target_full_name, fontsize='medium')  # type: ignore
            # save the data per model per target
            if self.save_path:
                df = pd.DataFrame({"s": idx[~mask],
                                   "x": embedding[~mask, 0],
                                   "y": embedding[~mask, 1],
                                   "target": y_true })
                filename = f"{self.model_name.replace(' ', '_').lower()}-{target_name}.csv"
                df.to_csv(self.save_path / filename)
        # set the header
        for fig in [fig_1, fig_2]:
            fig.suptitle(f"{self.model_name}\n"
                         "UMAP on latent representation z")
            yield fig
        # # split data according to habitat
        # fig_3 = plt.figure(figsize=(11.69, 8.27), dpi=100)
        # grid_spec = gs.GridSpec(7, 6,
        #                         hspace=0.5,
        #                         width_ratios=[0.04, 1.0, 1.0, 1.0, 1.0, 0.5],
        #                         height_ratios=[0.04, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
        # for i, (target_name, target_attrs) in enumerate(self.targets.items()):
        #     ax = fig_3.add_subplot(grid_spec[0, i + 1])
        #     ax.set_title(str(target_attrs["full_name"]), fontsize="medium")
        #     ax.set_frame_on(False)  # type: ignore
        #     ax.set_xticks([])
        #     ax.set_yticks([])
        # for i, habitat_label in enumerate(np.unique(ys[:, 0])):
        #     label = int(habitat_label)
        #     habitat_name = self.decoder["habitat"][label]
        #     habitat_idx = np.where(ys[:, 0] == label)[0]
        #     title_ax = fig_3.add_subplot(grid_spec[i + 1, 0])
        #     title_ax.set_ylabel(habitat_name, rotation=90)
        #     title_ax.set_frame_on(False)  # type: ignore
        #     title_ax.set_xticks([])
        #     title_ax.set_yticks([])
        #     # plot unique sites per habitat for species/time/habitat labels
        #     # so we get a UMAP breakdown per habitat separation by species metrics and time
        #     for j, (target_name, target_attrs) in enumerate(self.targets.items()):
        #         ax = fig_3.add_subplot(grid_spec[i + 1, j + 1])
        #         target_i, target_full_name = (int(target_attrs["index"]),
        #                                       str(target_attrs["full_name"]))
        #         # extract all sites / recordings from this habitat
        #         y_true = ys[habitat_idx, target_i]
        #         # fetch the labels for each site
        #         site_labels = [str(self.decoder["site"][int(y)]) for y in ys[habitat_idx, 1]]
        #         # create a colormap
        #         colour_key = cm.get_cmap(self.cmap)(np.linspace(0, 1, len(site_labels)))  # type: ignore
        #         # give each site a unique colour
        #         label_colour_mapping = {k: colors.to_hex(colour_key[l])
        #                                 for l, k in enumerate(site_labels)}
        #         colours = [str(label_colour_mapping[label])
        #                    for label in site_labels]
        #         # generate legend labels
        #         legend_elements = [Patch(facecolor=colour_key[k], label=site_labels[k])
        #                            for k, _ in enumerate(site_labels)]
        #         # plot points
        #         ax.scatter(embedding[habitat_idx, 0], embedding[habitat_idx, 1],
        #                    marker='.',
        #                    label=site_labels,  # type: ignore
        #                    c=colours)  # type: ignore
        #         ax.get_yaxis().set_visible(False)  # type: ignore
        #         ax.get_xaxis().set_visible(False)  # type: ignore
        #         ax.set_xticks([])
        #         ax.set_yticks([])
        #     # move legend to top right
        #     ax.legend(handles=legend_elements,
        #               bbox_to_anchor=(1, 1),
        #               loc="upper left",
        #               ncol=3)
        # for fig in [fig_1, fig_2, fig_3]:
            # yield fig

    def cache_data(self,
                   output: Dict[str, torch.Tensor]) -> None:
        for z, targets, idx in zip(output["z"],
                                   output["y"],
                                   output["s"]):
            self.cache["z"].append(z.cpu().numpy())
            self.cache["y"].append(targets.cpu().numpy())
            self.cache["s"].append(idx.cpu().numpy())

    def reset_cache(self):
        # clear the cache
        self.cache: Dict[str, List] = {"z": [], "y": [], "s": [] }
