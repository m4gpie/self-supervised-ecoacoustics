from typing import Callable, List, Dict, Tuple, Union, Any, Iterator

import wandb  # type: ignore
import torch  # type: ignore
import numpy as np
import pytorch_lightning as pl  # type: ignore
from matplotlib.figure import Figure  # type: ignore

from conduit.data import TernarySample  # type: ignore
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.utils import generate_title_string, mel_spectrogram_params
from ecoacoustics.types import Stage

class Reconstructor(pl.Callback):
    def __init__(self,
                 frame_transform: Frame,
                 targets: Dict[str, Dict[str, Union[str, int]]],
                 decoder: Dict[str, Any],
                 reconstruct_every_n_steps: int = 50,
                 specgram_params: Dict[str, Any] = mel_spectrogram_params()) -> None:
        self.frame_transform = frame_transform
        self.decoder = decoder
        self.targets = targets
        self.reconstruct_every_n_steps = reconstruct_every_n_steps
        self.specgram_params = specgram_params

    def on_train_batch_end(self,
                           trainer: pl.Trainer,
                           pl_module: pl.LightningModule,
                           outputs: Dict[str, torch.Tensor],
                           batch: TernarySample,
                           batch_idx: int) -> None:
        from matplotlib import pyplot as plt
        if trainer.global_step % self.reconstruct_every_n_steps == 0:
            output = {key: outputs[key][0].unsqueeze(0)
                      for key in outputs.keys()
                      if key in ['x', 'x_hat', 'x_log_var', 'y', 's'] }
            for fig in self.to_figures(output):
                pl_module.logger.experiment.log({"train/spectrograms": wandb.Image(fig) })
                plt.close(fig)

    def on_validation_batch_end(self,
                                trainer: pl.Trainer,
                                pl_module: pl.LightningModule,
                                outputs: Dict[Stage, Dict[str, torch.Tensor]],
                                batch: TernarySample,
                                batch_idx: int,
                                dataloader_idx: int) -> None:
        from matplotlib import pyplot as plt
        output = {key: outputs[Stage.validate][key][0].unsqueeze(0)
                  for key in outputs[Stage.validate].keys()
                  if key in ['x', 'x_hat', 'x_log_var', 'y', 's'] }
        for fig in self.to_figures(output):
            pl_module.logger.experiment.log({"val/spectrograms": wandb.Image(fig) })
            plt.close(fig)

    def to_figures(self,
                   outputs: Dict[str, torch.Tensor]) -> Iterator[Figure]:
        # lazyload modules
        import librosa  # type: ignore
        import numpy as np
        from librosa import display as libd
        from matplotlib import pyplot as plt
        from matplotlib import gridspec as gs  # type: ignore
        # extract all outputs
        xs, x_hats, ys, s = [outputs[key] for key in ["x", "x_hat", "y", "s"]]
        # if x_log_var is present, we're using the VAE not the AE
        # so we can also plot the uncertainty
        if "x_log_var" in outputs:
            x_stds = (0.5 * outputs["x_log_var"]).exp()
            num_rows = 3
        else:
            num_rows = 2
        # create and yield a figure for each observation
        for j, (x, x_hat, y, i) in enumerate(zip(xs, x_hats, ys, s)):
            # create a figure and grid spec
            fig = plt.figure(figsize=(11.69, 8.27), dpi=100)
            grid_spec = gs.GridSpec(num_rows, 2,
                                    width_ratios=[1, 0.04],
                                    hspace=0.8)
            y = y.cpu().numpy()
            x = x.unsqueeze(0).cpu()
            x_hat = x_hat.unsqueeze(0).cpu()
            # remove channel dimension, merge frames,
            # invert the log on the mel for x and x_hat
            x = self.frame_transform.backward(x).exp()
            x_hat = self.frame_transform.backward(x_hat).exp()
            # librosa works on the cpu, convert to db range
            x = librosa.amplitude_to_db(x.numpy())
            x_hat = librosa.amplitude_to_db(x_hat.numpy())
            # get min/max values for colourbar boundaries
            v_min = min(x.min(), x_hat.min())
            v_max = max(x.max(), x_hat.max())
            # plot observation, reconstruction and uncertainty spectrograms
            ax1 = fig.add_subplot(grid_spec[0, 0])
            ax2 = fig.add_subplot(grid_spec[1, 0])
            # render the original input
            mesh_1 = libd.specshow(x,
                                   vmin=v_min,
                                   vmax=v_max,
                                   ax=ax1,
                                   **self.specgram_params)
            plt.colorbar(mesh_1,  # type: ignore
                         format='%+3.1f dB',
                         cax=fig.add_subplot(grid_spec[0, 1]),
                         orientation="vertical")
            ax1.set_title("Input Mel Spectrogram", fontsize='medium')  # type: ignore
            # and the reconstruction
            mesh_2 = libd.specshow(x_hat,
                                   vmin=v_min,
                                   vmax=v_max,
                                   ax=ax2,
                                   **self.specgram_params)
            plt.colorbar(mesh_2,  # type: ignore
                         format='%+3.1f dB',
                         cax=fig.add_subplot(grid_spec[1, 1]),
                         orientation="vertical")
            ax2.set_title("Reconstructed Mel Spectrogram", fontsize='medium')  # type: ignore
            if "x_log_var" in outputs:
                # and the variance
                ax3 = fig.add_subplot(grid_spec[2, 0])
                x_std = x_stds[j].unsqueeze(0).cpu()
                x_std = self.frame_transform.backward(x_std)
                x_std = x_std.exp()
                x_std = librosa.amplitude_to_db(x_std.numpy())
                mesh_3 = libd.specshow(x_std, ax=ax3, **self.specgram_params)
                # add a colourbar
                plt.colorbar(mesh_3,  # type: ignore
                             format='%+3.1f dB',
                             cax=fig.add_subplot(grid_spec[2, 1]),
                             orientation="vertical")
                ax3.set_title("Uncertainty Spectrogram", fontsize='medium')  # type: ignore
            # set a title
            suptitle = generate_title_string(self.decoder, self.targets, y, int(i))
            fig.suptitle(suptitle, wrap=True) # type: ignore
            yield fig
