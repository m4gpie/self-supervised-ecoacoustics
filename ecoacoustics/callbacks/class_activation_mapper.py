from typing import List, Dict, Any, Union, Iterator

import wandb  # type: ignore
import torch  # type: ignore
import numpy as np
import pytorch_lightning as pl  # type: ignore

from pytorch_grad_cam import GradCAM  # type: ignore
from pytorch_grad_cam.utils.image import show_cam_on_image  # type: ignore
from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget  # type: ignore

from matplotlib import pyplot as plt
from matplotlib.figure import Figure  # type: ignore
from matplotlib.colors import ListedColormap  # type: ignore

from conduit.data import TernarySample  # type: ignore
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.types import Stage
from ecoacoustics.utils import generate_title_string

class ClassActivationMapper(pl.Callback):
    def __init__(self,
                 targets: Dict[str, Dict[str, Union[str, int]]],
                 frame_transform: Frame,
                 decoder: Dict[str, Dict[int, Any]],
                 sample_rate: int = 16_000,
                 stft_hop_length_seconds: float = 0.025) -> None:
        super().__init__()
        self.targets = targets
        self.frame_transform = frame_transform
        self.decoder = decoder
        self.sample_rate = sample_rate
        self.hop_length = round(sample_rate * stft_hop_length_seconds)

    def on_predict_batch_end(self,
                             trainer: pl.Trainer,
                             pl_module: pl.LightningModule,
                             outputs: Dict[Stage, Dict[str, torch.Tensor]],
                             batch: TernarySample,
                             batch_idx: int,
                             dataloader_idx: int) -> None:
        model = pl_module.encoder
        for fig in self.to_figures(model, batch):
            pl_module.logger.experiment.log({ "predict/class-activation-maps": wandb.Image(fig) })
            plt.close(fig)

    def to_figures(self,
                   model: torch.nn.Module,
                   batch: TernarySample) -> Iterator[Figure]:
        # lazyload librosa
        import librosa  # type: ignore
        import numpy as np
        from matplotlib import pyplot as plt
        from matplotlib import cm
        from matplotlib import gridspec as gs  # type: ignore
        from matplotlib.ticker import Formatter, ScalarFormatter  # type: ignore
        from matplotlib.ticker import LogLocator, FixedLocator, MaxNLocator
        from matplotlib.ticker import SymmetricalLogLocator
        # extract last convolutional layer
        target_layers = [model.features[-3]]
        # create a class activation mapper
        cam = GradCAM(model=model,
                      target_layers=target_layers)
        # extract spectrograms
        xs, ys, sample_idx = batch
        x_flats = xs.view(-1, 1, xs.size(3), xs.size(4))
        # TODO: figure out how to do per-category activations
        # y_values = batch_output[Stage.test]["y"]
        # # extract categorical target values
        # targets = []
        # for target in self.targets:
        #     # only handle categorical targets
        #     if target["type"] == "categorical":
        #         # extract the target index
        #         target_i = int(target["index"])
        #         # fetch values and cast as integer
        #         target_values = y_values[:, target_i].long()
        #         # create a classifier output target
        #         targets.append(ClassifierOutputTarget(target_values))
        # create greyscale class activation map
        greyscale_cam = cam(input_tensor=x_flats)
                            # targets=targets)
        # make cams a tensor and unflatten batch and frame dimensions
        cams = torch.as_tensor(greyscale_cam).view(*xs.shape)
        # yield each observation
        for x, y, cam, i in zip(xs, ys, cams, sample_idx):
            # unframe, invert the mel, move to numpy
            y = y.cpu().numpy()
            mel_spectrogram = self.frame_transform.backward(x.cpu()).exp().numpy()
            class_activation_map = self.frame_transform.backward(cam.cpu()).numpy()
            # create a grid spec, figure and axes
            grid_spec = gs.GridSpec(2, 2, width_ratios=[1, 0.03], hspace=0.4)
            fig = plt.figure(figsize=(11.69, 8.27), dpi=100)
            ax1 = fig.add_subplot(grid_spec[0, 0])
            ax2 = fig.add_subplot(grid_spec[1, 0])
            ax3 = fig.add_subplot(grid_spec[0, 1])
            ax4 = fig.add_subplot(grid_spec[1, 1])
            # create x-axis and y-axis using librosa's helper methods for time & mel
            xs = librosa.core.frames_to_time(np.arange(mel_spectrogram.shape[1]),
                                             sr=self.sample_rate,
                                             hop_length=self.hop_length)
            ys = librosa.core.mel_frequencies(mel_spectrogram.shape[0], 125, 7500)
            # plot spectrogram as decibels
            mel_db = librosa.amplitude_to_db(mel_spectrogram)
            # plot the spectrogram alone
            mesh_1 = ax1.pcolormesh(xs, ys,  # type: ignore
                                    mel_db,
                                    cmap='viridis',
                                    shading='auto')
            # add colourbar for spectrogram
            fig.colorbar(mesh_1,  # type: ignore
                         format='%+3.1f dB',
                         cax=ax3,
                         orientation="vertical")
            # plot it again on the lower one, which we'll
            # plot over with the activation maps
            ax2.pcolormesh(xs, ys,  # type: ignore
                           mel_db,
                           cmap='viridis',
                           shading='auto')
            # create a sigmoidal activation colourmap overlay so all
            # low values fade to blank and the original plot is visible
            original_cmap = cm.get_cmap('Reds')  # type: ignore
            sigmoid_cmap = original_cmap(np.arange(original_cmap.N))
            x = np.linspace(-10, 10, original_cmap.N)
            sigmoid_cmap[:,-1] = np.exp(x) / (np.exp(x) + 1)
            sigmoid_cmap = ListedColormap(sigmoid_cmap)
            # plot activation
            mesh_2 = ax2.pcolormesh(xs, ys,  # type: ignore
                                    class_activation_map,
                                    vmax=1.0,
                                    vmin=0.0,
                                    cmap=sigmoid_cmap,
                                    shading='auto')
            # add colourbar for class activation
            fig.colorbar(mesh_2,  # type: ignore
                         cax=ax4,
                         orientation="vertical")
            # set scale for time and mel axes
            # xscale defaults to linear
            # yscale is non-linear, so we make compatible with the mel scale
            for ax in [ax1, ax2]:
                y_axis = ax.yaxis  # type: ignore
                ax.set_yscale("symlog", linthresh=1000.0, base=2)  # type: ignore
                y_axis.set_major_formatter(ScalarFormatter())
                y_axis.set_major_locator(SymmetricalLogLocator(y_axis.get_transform()))
                ax.set_ylabel("Hz")
                ax.set_xlabel("Time (s)")
            # generate titie containing label values
            ax1.set_title("Input Mel Spectrogram", fontsize="medium")  # type: ignore
            ax2.set_title("Class Activation Map", fontsize="medium")  # type: ignore
            # append to save
            fig.suptitle(generate_title_string(self.decoder, self.targets, y, int(i)))
            yield fig
