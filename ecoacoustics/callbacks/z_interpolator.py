from typing import Dict, List, Sequence, Any, Union, Callable, Tuple
from numpy import typing as npt

import random
import torch  # type: ignore
import wandb  # type: ignore
import numpy as np
import pytorch_lightning as pl  # type: ignore
from matplotlib import gridspec as gs  # type: ignore
from matplotlib.figure import Figure  # type: ignore
from matplotlib.colors import Colormap  # type: ignore
from matplotlib.ticker import Formatter, ScalarFormatter  # type: ignore
from matplotlib.ticker import LogLocator, FixedLocator, MaxNLocator
from matplotlib.ticker import SymmetricalLogLocator

from conduit.data import TernarySample  # type: ignore
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.utils import generate_title_string, mel_spectrogram_params

class ZInterpolator(pl.Callback):
    def __init__(self,
                 targets: Dict[str, Dict[str, Union[str, int]]],
                 frame_transform: Frame,
                 decoder: Dict[str, Any],
                 grid_layout: Tuple = (5, 5),
                 specgram_params: Dict[str, Any] = mel_spectrogram_params()):
        super().__init__()
        self.targets = targets
        self.frame_transform = frame_transform
        self.grid_layout = grid_layout
        self.label_decoder = decoder
        self.num_steps = np.array(grid_layout).prod()
        self.specgram_params = specgram_params
        self.specgram_params["cmap"] = 'viridis'

    def on_test_batch_end(self,
                          trainer: pl.Trainer,
                          pl_module: pl.LightningModule,
                          outputs: Dict,
                          batch: TernarySample,
                          batch_idx: int) -> None:
        fig = self.interpolate(pl_module.model.decoder, outputs)
        image = wandb.Image(fig)
        pl_module.logger.experiment.log({"test/interpolation": image })

    def interpolate(self,
                    decoder: torch.nn.Module,
                    outputs: Dict[str, torch.Tensor]) -> Figure:
        import librosa  # type: ignore
        import numpy as np
        from librosa import display as libd
        from matplotlib import pyplot as plt
        # create a figure
        fig = plt.figure(figsize=(11.69, 8.27), dpi=100)
        # create two grid of images, one column, n rows,
        # one for mean, one for variance
        num_rows, num_cols = self.grid_layout[0], self.grid_layout[1]
        outer_grid = gs.GridSpec(2, 3,
                                 width_ratios=[0.01, 1.0, 0.04],
                                 height_ratios=[1.0, 0.01])
        inner_grid = gs.GridSpecFromSubplotSpec(num_rows, num_cols,
                                                subplot_spec=outer_grid[1],
                                                wspace=0.0,
                                                hspace=0.0)
        zs, ys, ss = outputs["z"], outputs["y"], outputs["s"]
        # pick two indices
        i, j = random.sample(list(range(len(zs))), 2)
        # extract the relevant spectrograms and z vectors
        z_1, z_2 = zs[i], zs[j]
        import code; code.interact(local=locals())
        y_1, y_2 = ys[i, 0], ys[j, 0]
        s_1, s_2 = ss[i], ss[j]
        habitat_decode = self.label_decoder["habitat"]
        l_1, l_2 = habitat_decode[int(y_1)], habitat_decode[int(y_2)]
        # each z is a 128 dimensional vector of n time-averaged floating points
        # sampled from learned distributions for each frame in an observation
        # to interpolate between observations, we create a continuous range of values
        # between them in the latent space, vectors are passed to the decoder to
        # generate a reconstructed frame
        interpolation = torch.as_tensor(np.linspace(z_1, z_2, self.num_steps),
                                        dtype=zs.dtype)
        # compute each interpolated reconstruction and variance spectrograms
        decoded = decoder(interpolation)
        x_hats, x_log_vars = decoded.chunk(2, dim=1)
        # find max and min vals on z-axis
        vmax = -np.inf
        vmin = np.inf
        for x_hat in x_hats:
            x_hat = x_hat.exp().numpy()
            mel_db = librosa.amplitude_to_db(x_hat)
            vmax = max(vmax, mel_db.max())
            vmin = min(vmin, mel_db.min())
        # using our interpolation, reconstruct x_hat based on time-averaged z
        for k, x_hat in enumerate(x_hats):
            row, col = int(k / num_rows), k % num_cols
            ax = fig.add_subplot(inner_grid[row, col])
            x_hat = x_hat.squeeze(0).exp().cpu().numpy().T
            mesh = libd.specshow(librosa.amplitude_to_db(x_hat),
                                 vmin=vmin,
                                 vmax=vmax,
                                 ax=ax,
                                 **self.specgram_params)
            # add a number to illustrate interpolation direction
            ax.text(0.9, 0.9, str(k + 1),
                    color='white',
                    fontsize=8,
                    transform=ax.transAxes)  # type: ignore
            if k == 0:
                ax.text(0.1, 0.1, str(l_1),
                        color='white',
                        fontsize=8,
                        transform=ax.transAxes)  # type: ignore
            if k == len(x_hats) - 1:
                ax.text(0.1, 0.1, str(l_2),
                        color='white',
                        fontsize=8,
                        transform=ax.transAxes)  # type: ignore
            # only add x and y axis on boundaries
            ax.set_ylabel("")
            ax.set_xlabel("")
            if col != 0:
                ax.get_yaxis().set_visible(False)  # type: ignore
            if row != (num_rows - 1):
                ax.get_xaxis().set_visible(False)  # type: ignore
            else:
                x_ticks = np.arange(0.24, 0.96 + 0.01, 0.24)
                ax.set_xticks(x_ticks)
                for tick in ax.get_xticklabels():
                    tick.set_rotation(45)
        # add single axes labels outside the grid
        x_label_ax = fig.add_subplot(outer_grid[1, :-1])
        x_label_ax.set_xlabel('Time (s)')
        x_label_ax.set_frame_on(False)
        x_label_ax.set_xticks([])
        x_label_ax.set_yticks([])
        y_label_ax = fig.add_subplot(outer_grid[:, 0])
        y_label_ax.set_ylabel('Frequency (Hz)')
        y_label_ax.set_frame_on(False)
        y_label_ax.set_xticks([])
        y_label_ax.set_yticks([])
        # add colourbar
        plt.colorbar(mesh, cax=fig.add_subplot(outer_grid[:-1, -1]), format='%+3.1f dB')   # type: ignore
        # TODO: do according to specified labels...
        fig.suptitle(f"Interpolation in the latent space between {l_1} to {l_2}\n")
        return fig


