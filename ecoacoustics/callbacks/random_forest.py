from typing import List, Dict, Tuple, Union, Any
from numpy import typing as npt

from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor  # type: ignore
from sklearn import metrics  # type: ignore

import torch  # type: ignore
import wandb  # type: ignore
import numpy as np
import pytorch_lightning as pl  # type: ignore

from matplotlib import pyplot as plt
from matplotlib.figure import Figure  # type: ignore
from matplotlib.colors import Colormap  # type: ignore

from conduit.data import TernarySample  # type: ignore
from conduit.data.datasets.audio.ecoacoustics import SoundscapeAttr  # type: ignore
from ecoacoustics.types import Stage
from ecoacoustics.utils import TargetsMixin

Cache = Dict[Stage, Dict[str, List[npt.NDArray]]]

class RandomForest(TargetsMixin, pl.Callback):
    def __init__(self,
                 targets: Dict[str, Dict[str, Union[str, int]]],
                 decoder: Dict[str, Any],
                 num_estimators: int = 100,
                 model_name: str = 'VAE',
                 cmap: Union[str, Colormap] = 'ocean') -> None:
        self.targets = targets
        super().__init__()
        self.decoder = decoder
        self.num_estimators = num_estimators
        self.model_name = model_name
        print("setting up secondary RF classifier")
        self.cmap = cmap
        self.reset_cache()

    def on_validation_batch_end(self,
                                trainer: pl.Trainer,
                                pl_module: pl.LightningModule,
                                output: Dict[Stage, Dict[str, torch.Tensor]],
                                batch: TernarySample,
                                batch_idx: int,
                                dataloader_idx: int) -> None:
        """
        lightning doesnt allow callbacks to receive outputs in on_validation_epoch_end,
        so we'll cache the results of each batch as we go inside the callback as recommended
        in their documentation
        """
        self.cache_data(output, Stage.validate)

    def on_validation_epoch_end(self,
                                trainer: pl.Trainer,
                                pl_module: pl.LightningModule) -> None:
        """
        at the end of a validation epoch, we fit our random forest to the
        """
        # extract datasplit
        train_data, test_data = self.preprocess(self.cache)
        # fit n random forests for each target category
        forests = self.fit(train_data)
        # compute predictions on the test data
        results = self.predict(forests, test_data)
        # log the f1 score for other early stopper / checkpointer
        pl_module.log("val/f1_score", results["f1_score"])
        # prepend keys with val string to keep W&B tidy
        validation_results = { f"val/{k}": v for (k, v) in results.items() }
        # for all categorical targets we've generated confusion matrices, so
        # we need to make them into wandb images
        for target_name in self.categorical_targets().keys():
            fig = validation_results[f"val/confusion/{target_name}"]
            # upload results
            pl_module.logger.experiment.log({ f"val/confusion/{target_name}": wandb.Image(fig) })
            # close figure
            plt.close(fig)
            # delete figure key from results
            del validation_results[f"val/confusion/{target_name}"]
        # upload results
        pl_module.logger.experiment.log(validation_results)
        # clear the cache
        self.reset_cache()

    def on_predict_batch_end(self,
                             trainer: pl.Trainer,
                             pl_module: pl.LightningModule,
                             output: Dict[Stage, Dict[str, torch.Tensor]],
                             batch: TernarySample,
                             batch_idx: int,
                             dataloader_idx: int) -> None:
        """
        lightning doesnt allow callbacks to receive outputs in on_validation_epoch_end,
        so we'll cache the results of each batch as we go inside the callback as recommended
        in their documentation
        """
        self.cache_data(output, Stage.test)

    def on_predict_epoch_end(self,
                             trainer: pl.Trainer,
                             pl_module: pl.LightningModule,
                             outputs) -> None:
        # extract data split
        train_data, test_data = self.preprocess(self.cache)
        # fit n random forests for each target category
        forests = self.fit(train_data)
        # compute predictions on the test data
        results = self.predict(forests, test_data)
        # prepend keys with val string to keep W&B tidy
        predict_results = { f"predict/{k}": v for (k, v) in results.items() }
        # for all categorical targets we've generated confusion matrices, so
        # we need to make them into wandb images
        for target_name in self.categorical_targets().keys():
            fig = predict_results[f"predict/confusion/{target_name}"]
            # upload results
            pl_module.logger.experiment.log({ f"predict/confusion/{target_name}": wandb.Image(fig) })
            # close figure
            plt.close(fig)
            # delete figure key from results
            del predict_results[f"predict/confusion/{target_name}"]
        # send remaining results to W&B
        pl_module.logger.experiment.log(predict_results)
        # clear the cache
        self.reset_cache()

    def fit(self,
            data: Tuple[npt.NDArray, npt.NDArray]) -> Dict[int, Union[RandomForestClassifier,
                                                                      RandomForestRegressor]]:
        # define a dictionary to contain fitted models
        random_forests: Dict[int, Union[RandomForestClassifier, RandomForestRegressor]] = {}
        # extract z and y training data
        z_train, y_train = data
        # execute a RF classification task for each label
        for target_name, target_attrs in self.categorical_targets().items():
            target_i = int(target_attrs["index"])
            # create a random forest classifier model
            random_forest = RandomForestClassifier(n_estimators=self.num_estimators)
            # fit to the training data
            random_forest.fit(z_train, y_train[:, target_i])
            # cache model for returning
            random_forests[target_i] = random_forest
        # execute a RF regression task for each continuous target
        for target_name, target_attrs in self.continuous_targets().items():
            target_i = int(target_attrs["index"])
            # generate a mask capturing null values (time values might be null)
            if not (mask := np.isnan(y_train[:, target_i])).all():
                # create a random forest regressor model
                random_forest = RandomForestRegressor(n_estimators=self.num_estimators)
                # fit to the training data if there are valid targets
                random_forest.fit(z_train[~mask], y_train[~mask, target_i])
                # cache model for returning
                random_forests[target_i] = random_forest
            else:
                random_forests[target_i] = None
        return random_forests

    def predict(self,
                models: Dict[int, Union[RandomForestClassifier, RandomForestRegressor]],
                data: Tuple[npt.NDArray, npt.NDArray]) -> Dict[str, Any]:
        # define a results dictionary for accuracy, f1 scores and confusion matrices
        results: Dict[str, Any] = {}
        # extract z and y test data
        z_test, y_test = data
        f1_score_avg = 0.0
        accuracy_avg = 0.0
        error_avg = 0.0
        num_categorical_targets = len(self.categorical_targets())
        num_continuous_targets = len(self.continuous_targets())
        # predict for all categorical targets
        for target_name, target_attrs in self.categorical_targets().items():
            fig = plt.figure(figsize=(11.69, 8.27), dpi=100)
            ax = fig.add_subplot(111)
            target_i, full_name, num_classes = (int(target_attrs["index"]),
                                                str(target_attrs["full_name"]),
                                                int(target_attrs["num_classes"]))
            # extract fitted RF model
            random_forest = models[target_i]
            # extract relevant target data
            y_true = y_test[:, target_i]
            # predict the target using the fitted model
            y_pred = random_forest.predict(z_test)
            # compute f1 and accuracy
            f1_score = metrics.f1_score(y_true, y_pred, average="micro")
            accuracy = metrics.accuracy_score(y_true, y_pred)
            # add to the mean
            f1_score_avg += f1_score / num_categorical_targets
            accuracy_avg += accuracy / num_categorical_targets
            # compile a confusion matrix
            labels = list(self.decoder[target_name].keys())
            display_labels = list(self.decoder[target_name].values())
            metrics.ConfusionMatrixDisplay.from_predictions(y_true, y_pred,
                                                            labels=labels,
                                                            display_labels=display_labels,
                                                            xticks_rotation=45,
                                                            ax=ax,
                                                            cmap=self.cmap)
            ax.set_title(f"F1 Score: {f1_score}")
            fig.suptitle(f"{self.model_name} Random Forest Confusion Matrix")
            # merge into results
            results[f"confusion/{target_name}"] = fig
            results[f"f1_score/{target_name}"] = f1_score
            results[f"accuracy/{target_name}"] = accuracy
        # predict for all continuous targets
        for target_name, target_attrs in self.continuous_targets().items():
            target_i = int(target_attrs["index"])
            # extract fitted RF model (if there is one)
            random_forest = models.get(target_i)
            # only skip the prediction when all targets are null
            if random_forest and not (mask := np.isnan(y_test[:, target_i])).all():
                # predict targets using fitted model if there are valid targets
                y_true = y_test[~mask, target_i]
                y_pred = random_forest.predict(z_test[~mask])
                # compute error
                error = metrics.mean_squared_error(y_true, y_pred)
                error_avg += error / num_continuous_targets
                # merge into results
                results[f"error/{target_name}"] = error
        # merge averages into results
        results["f1_score"] = float(f1_score_avg)
        results["accuracy"] = float(accuracy_avg)
        results["error"] = float(error_avg)
        return results

    def preprocess(self,
                   outputs: Dict[Stage, Dict[str, List]]) -> Tuple[Tuple[npt.NDArray, npt.NDArray],
                                                                   Tuple[npt.NDArray, npt.NDArray]]:
        # extract training data
        train_idx = np.stack(outputs[Stage.train]["s"])
        train_idx, train_idx_mask = np.unique(train_idx, return_index=True)
        z_train = np.stack(outputs[Stage.train]["z"])[train_idx_mask]
        y_train = np.stack(outputs[Stage.train]["y"])[train_idx_mask]
        # and test data
        test_idx = np.stack(outputs[Stage.test]["s"])
        test_idx, test_idx_mask = np.unique(test_idx, return_index=True)
        z_test = np.stack(outputs[Stage.test]["z"])[test_idx_mask]
        y_test = np.stack(outputs[Stage.test]["y"])[test_idx_mask]
        # bundle and return
        train_data = (z_train, y_train)
        test_data = (z_test, y_test)
        return train_data, test_data

    def cache_data(self,
                   output: Dict[Stage, Dict[str, torch.Tensor]],
                   stage: Stage) -> None:
        # cache the training data on the cpu
        train_output = output[Stage.train]
        for z, targets, idx in zip(train_output["z"],
                                   train_output["y"],
                                   train_output["s"]):
            self.cache[Stage.train]["z"].append(z.cpu().numpy())
            self.cache[Stage.train]["y"].append(targets.cpu().numpy())
            self.cache[Stage.train]["s"].append(idx.cpu().numpy())
        # cache the test data on the cpu
        test_output = output[stage]
        for z, targets, idx in zip(test_output["z"],
                                   test_output["y"],
                                   test_output["s"]):
            self.cache[Stage.test]["z"].append(z.cpu().numpy())
            self.cache[Stage.test]["y"].append(targets.cpu().numpy())
            self.cache[Stage.test]["s"].append(idx.cpu().numpy())

    def reset_cache(self):
        # clear the cache
        self.cache: Cache = {Stage.train: {"z": [], "y": [], "s": [] },
                             Stage.test: {"z": [], "y": [], "s": [] } }
