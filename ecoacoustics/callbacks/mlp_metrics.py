from typing import List, Dict, Tuple, Union, Any
from numpy import typing as npt

import numpy as np
import torch  # type: ignore
import pytorch_lightning as pl  # type: ignore
import wandb  # type: ignore

from torch.functional import F  # type: ignore
from torchmetrics import functional as M  # type: ignore
from torchmetrics import ConfusionMatrix
from sklearn import metrics  # type: ignore
from matplotlib import pyplot as plt
from matplotlib.figure import Figure  # type: ignore
from matplotlib.colors import Colormap  # type: ignore
from conduit.data import TernarySample  # type: ignore
from ecoacoustics.types import Stage
from ecoacoustics.utils import TargetsMixin

class MLPMetrics(TargetsMixin, pl.Callback):
    def __init__(self,
                 targets: Dict[str, Dict[str, Union[str, int]]],
                 decoder: Dict[str, Any],
                 cmap: Union[str, Colormap] = 'ocean') -> None:
        self.targets = targets
        super().__init__()
        self.decoder = decoder
        self.cmap = cmap
        self.reset_cache()

    def on_validation_batch_end(self,
                                trainer: pl.Trainer,
                                pl_module: pl.LightningModule,
                                output: Dict[Stage, Dict[str, torch.Tensor]],
                                batch: TernarySample,
                                batch_idx: int,
                                dataloader_idx: int) -> None:
        self.cache_data(output, Stage.validate)

    def on_validation_epoch_end(self,
                                trainer: pl.Trainer,
                                pl_module: pl.LightningModule) -> None:
        """
        at the end of a validation epoch, we fit our random forest to the
        """
        # stack cached tensors for each shared key
        outputs = self.preprocess(self.cache, Stage.validate)
        # compute predictions on the val data
        results = self.predict(outputs[Stage.validate])
        # log the f1 score for other early stopper / checkpointer
        pl_module.log("val/f1_score", results["f1_score"])
        # prepend keys with val string to keep W&B tidy
        validation_results = { f"val/{k}": v for (k, v) in results.items() }
        # for all categorical targets we've generated confusion matrices, so
        # we need to make them into wandb images
        for target_name in self.categorical_targets().keys():
            fig = validation_results[f"val/confusion/{target_name}"]
            # upload results
            pl_module.logger.experiment.log({ f"val/confusion/{target_name}": wandb.Image(fig) })
            # close figure
            plt.close(fig)
            # delete figure key from results
            del validation_results[f"val/confusion/{target_name}"]
        # upload results
        pl_module.logger.experiment.log(validation_results)
        # clear the cache
        self.reset_cache()

    def on_predict_batch_end(self,
                             trainer: pl.Trainer,
                             pl_module: pl.LightningModule,
                             output: Dict[Stage, Dict[str, torch.Tensor]],
                             batch: TernarySample,
                             batch_idx: int,
                             dataloader_idx: int) -> None:
        """
        lightning doesnt allow callbacks to receive outputs in on_validation_epoch_end,
        so we'll cache the results of each batch as we go inside the callback as recommended
        in their documentation
        """
        self.cache_data(output, Stage.test)

    def on_predict_epoch_end(self,
                             trainer: pl.Trainer,
                             pl_module: pl.LightningModule,
                             outputs) -> None:
        # stack cached tensors for each shared key
        outputs = self.preprocess(self.cache, Stage.test)
        # compute predictions on the test data
        results = self.predict(outputs[Stage.test])
        # prepend keys with val string to keep W&B tidy
        predict_results = { f"predict/{k}": v for (k, v) in results.items() }
        # for all categorical targets we've generated confusion matrices, so
        # we need to make them into wandb images
        for target_name in self.categorical_targets().keys():
            fig = predict_results[f"predict/confusion/{target_name}"]
            # upload results
            pl_module.logger.experiment.log({ f"predict/confusion/{target_name}": wandb.Image(fig) })
            # close figure
            plt.close(fig)
            # delete figure key from results
            del predict_results[f"predict/confusion/{target_name}"]
        # send remaining results to W&B
        pl_module.logger.experiment.log(predict_results)
        # clear the cache
        self.reset_cache()

    def predict(self,
                output: Dict[str, torch.Tensor]):
        # define a results dictionary for accuracy, f1 scores and confusion matrices
        results: Dict[str, Any] = {}
        # extract y
        y = output["y"]
        # define average scores
        f1_score_avg = 0.0
        accuracy_avg = 0.0
        error_avg = 0.0
        num_categorical_targets = len(self.categorical_targets())
        num_continuous_targets = len(self.continuous_targets())
        # interate over all categorical targets
        for target_name, target_attrs in self.categorical_targets().items():
            target_i, full_name, num_classes = (int(target_attrs["index"]),
                                                str(target_attrs["full_name"]),
                                                int(target_attrs["num_classes"]))
            # extract true values
            y_true = y[:, target_i].long()
            # extract target values
            y_hat = output[f"logits/{target_name}"]
            # calculate prediction probabilities
            pred_probs = F.softmax(y_hat, dim=1)
            # then the class prediction
            _, y_pred = pred_probs.max(axis=1)
            # compute f1 and accuracy
            f1_score = M.f1_score(y_pred, y_true, average="micro").item()
            accuracy = M.accuracy(y_pred, y_true).item()
            # add to the mean
            f1_score_avg += f1_score / num_categorical_targets
            accuracy_avg += accuracy / num_categorical_targets
            # construct a confusion matrix
            labels = list(self.decoder[target_name].keys())
            display_labels = list(self.decoder[target_name].values())
            # create a figure and axis
            fig = plt.figure(figsize=(11.69, 8.27), dpi=100)
            ax = fig.add_subplot(111)
            # plot the matrix using sklearn
            metrics.ConfusionMatrixDisplay.from_predictions(y_pred.cpu().numpy(),
                                                            y_true.cpu().numpy(),
                                                            labels=labels,
                                                            display_labels=display_labels,
                                                            xticks_rotation=45,
                                                            ax=ax,
                                                            cmap=self.cmap)
            ax.set_title(f"F1 Score: {f1_score}")
            fig.suptitle("VAE MLP Confusion Matrix")
            # merge into results
            results[f"confusion/{target_name}"] = fig
            results[f"f1_score/{target_name}"] = f1_score
            results[f"accuracy/{target_name}"] = accuracy
        for target_name, target_attrs in self.continuous_targets().items():
            target_i, full_name = (int(target_attrs["index"]),
                                   str(target_attrs["full_name"]))
            y_true = y[:, target_i].numpy().flatten()
            # extract true values
            if not (mask := np.isnan(y_true)).all():
                # extract target values
                y_hat = output[f"logits/{target_name}"].numpy()
                # compute the mean squared error
                error = metrics.mean_squared_error(y_true[~mask], y_hat[~mask])
                # add to the average
                error_avg += error / num_continuous_targets
                # store discrete per-target error
                results[f"error/{target_name}"] = error
        # merge averages into results
        results["f1_score"] = float(f1_score_avg)
        results["accuracy"] = float(accuracy_avg)
        results["error"] = float(error_avg)
        return results

    def preprocess(self,
                   cached: Dict[Stage, Dict[str, List]],
                   stage: Stage) -> Dict[Stage, Dict[str, torch.Tensor]]:
        cached[stage]["y"] = torch.vstack(cached[stage]["y"])
        for target_name in self.categorical_targets().keys():
            key = f"logits/{target_name}"
            cached[stage][key] = torch.vstack(cached[stage][key])
        for target_name in self.continuous_targets().keys():
            key = f"logits/{target_name}"
            cached[stage][key] = torch.hstack(cached[stage][key])
        return cached

    def cache_data(self,
                   outputs: Dict[Stage, Dict[str, torch.Tensor]],
                   stage: Stage) -> None:
        # cache the data on the cpu
        self.cache[stage]["y"].append(outputs[stage]["y"].cpu())
        for target_name in self.categorical_targets().keys():
            key = f"logits/{target_name}"
            values = outputs[stage][key]
            self.cache[stage][key].append(values.cpu())
        for target_name in self.continuous_targets().keys():
            key = f"logits/{target_name}"
            mean_values, log_variance_values = outputs[stage][key]
            self.cache[stage][key].append(mean_values.cpu())

    def reset_cache(self):
        # clear the cache
        self.cache: Dict[Stage, Dict[str, List]] = {Stage.validate: {"y": [],
                                                                     **{f"logits/{target_name}": []
                                                                        for target_name in self.categorical_targets().keys() },
                                                                     **{f"logits/{target_name}": []
                                                                        for target_name in self.continuous_targets().keys() } },
                                                    Stage.test: {"y": [],
                                                                 **{f"logits/{target_name}": []
                                                                    for target_name in self.categorical_targets().keys() },
                                                                 **{f"logits/{target_name}": []
                                                                    for target_name in self.continuous_targets().keys() } } }
