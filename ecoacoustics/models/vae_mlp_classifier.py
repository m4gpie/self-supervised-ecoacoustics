from typing import Dict, List, Tuple, Union, Any
from numpy import typing as npt
import numpy as np
import torch  # type: ignore
from torch import nn  # type: ignore
from torch.functional import F  # type: ignore
import torchmetrics.functional as metrics  # type: ignore
import pytorch_lightning as pl  # type: ignore

from conduit.data import TernarySample  # type: ignore
from ecoacoustics.models.encoder import Encoder
from ecoacoustics.models.decoder import Decoder
from ecoacoustics.models.vae import VAE
from ecoacoustics.optimisers.step_kl import StepKL
from ecoacoustics.types import Stage
from ecoacoustics.utils import TargetsMixin

Outputs = Dict[str, Union[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]]

class VAEMLPClassifier(TargetsMixin, pl.LightningModule):
    def __init__(self,
                 checkpoint_path: str,
                 learning_rate: float,
                 targets: Dict[str, Dict[str, Any]]) -> None:
        """
        args:
          - learning_rate: rate of learning used in optimiser
          - targets: a list of dictionaries containing target information
        """
        self.targets = targets
        super().__init__()
        self.learning_rate = learning_rate
        # hack due to badly saved checkpoints / structured code
        # checkpoint = torch.load(checkpoint_path,
        #                         map_location=torch.device('cuda' if torch.cuda.is_available() else 'cpu'))
        # load hparams
        # hparams = checkpoint["hyper_parameters"]
        self.latent_features = 128# checkpoint["hyper_parameters"]["latent_features"]
        encoder = Encoder(out_features=self.latent_features,
                          vggish_pretrain=False,
                          vggish_frozen=False,
                          num_outputs=2)
        decoder = Decoder(in_features=self.latent_features,
                          out_channels=2,
                          negative_slope=0.03)
        self.model = VAE(encoder=encoder,
                         decoder=decoder)
        print("Setting up pretrained VAE")
        # self.load_state_dict(checkpoint["state_dict"])
        # setup secondary classifier MLP with one hidden layer
        # input is latent feature vector sampled from surrogate posterior
        print("Setting up secondary MLP classifier")
        self.mlp = nn.Sequential(nn.Linear(in_features=self.latent_features, out_features=512),
                                 nn.LeakyReLU(negative_slope=0.03, inplace=True))
        # setup linear output layer for each category
        self.classify = nn.ModuleList([nn.Linear(in_features=512, out_features=int(target_attrs["num_classes"]))
                                       for target_attrs in self.categorical_targets().values()])
        # setup a linear layer with two outputs for each variable, a mean and variance
        self.regress_mean = nn.Linear(in_features=512, out_features=len(self.continuous_targets()))
        self.regress_log_variance = nn.Linear(in_features=512, out_features=len(self.continuous_targets()))
        # initialize the model
        self.save_hyperparameters()

    def forward(self,
                x: torch.Tensor) -> Outputs:
        """
        a full forward pass of the VAE and MLP for the batch

        args:
          - x: a full batch of data x of dimensions (n, m, 1 96, 64), where n = batch_size,
              m = number of frames, 1 = number of channels, 96 = time axis, 64 = frequency axis,
              i.e. number of mel frequency bins

        returns:
          - x_hat: a full batch of reconstructed data x_hat of dimensions (n, 1, m, 1 96, 64)
          - mean: a full batch of compressed data as the latent mean of dimensions (n, m, 128)
          - log_variance: a full batch of compressed data as the latent log variance of dimensions (n, m, 128)
          - z: a full batch of sampled latent variables from learned distributions of dimensions (n, m, 128)
          - outputs: output values for each target variable
        """
        outputs = self.model(x)
        # compute hidden layer
        h = self.mlp(outputs["z"])
        outputs = self.logits(h, outputs)
        # return all data
        return outputs

    def logits(self,
               h: torch.Tensor,
               outputs: Outputs) -> Outputs:
        # compute multi-class categorical objectives one at a time
        for target_name, classify in zip(self.categorical_targets().keys(), self.classify):
            outputs[f"logits/{target_name}"] = classify(h)
        # compute mean and variance of continuous outputs
        regress_mean = self.regress_mean(h)
        regress_log_variance = self.regress_log_variance(h)
        for i, target_name in enumerate(self.continuous_targets().keys()):
            log_variance = regress_log_variance[:, i]
            # constrain species abundance / richness to be positive
            mean = F.softplus(regress_mean[:, i]) if target_name in ['NN', 'N0'] else regress_mean[:, i]
            outputs[f"logits/{target_name}"] = (mean, log_variance)
        return outputs

    def loss(self,
             y: torch.Tensor,
             outputs: Outputs) -> Dict[str, torch.Tensor]:
        outputs.update({"cross_entropy": 0.0, "nll": 0.0 })
        # calculate cross entropy loss for categorical targets
        for i, (target_name, target_attrs) in enumerate(self.categorical_targets().items()):
            target_i = int(target_attrs["index"])
            # fetch target labels and corresponding output values
            y_true = y[:, target_i].long()
            y_hat = outputs[f"logits/{target_name}"]
            # calculate the cross-entropy loss
            loss = F.cross_entropy(y_hat, y_true)
            # store for plots
            outputs["cross_entropy"] += loss.item()
            outputs[f"cross_entropy/{target_name}"] = loss.item()
            # sum with running loss
            outputs["loss"] += loss
        # regress on continuous targets using gaussian negative log likelihood of data
        for i, (target_name, target_attrs) in enumerate(self.continuous_targets().items()):
            target_i = int(target_attrs["index"])
            # fetch target labels and corresponding output values
            y_true = y[:, target_i]
            y_hat_mean, y_hat_log_var = outputs[f"logits/{target_name}"]
            # generate a mask capturing null values (time values can be null)
            if not (mask := torch.isnan(y_true)).any():
                # calculate the gaussian log probability w/o masked values
                loss = F.gaussian_nll_loss(y_hat_mean[~mask],
                                           y_true[~mask].float(),
                                           y_hat_log_var[~mask].exp())
                # store for plots
                outputs["nll"] += loss.item()
                outputs[f"nll/{target_name}"] = loss.item()
                # sum with running loss
                outputs["loss"] += loss
        return outputs

    def training_step(self,
                      batch: TernarySample,
                      batch_idx: int) -> Dict[str, torch.Tensor]:
        """
        training loop function called by pytorch lightning trainer

        args:
          - batch: a single batch is a binary sample, composed of data x of 
              dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch
        """
        # extract x, convert double to float
        # and squash along extraneous dimension
        x, y, s = batch
        # do a forward pass
        outputs = self.forward(x.float().squeeze(1))
        # calculate each loss and sum with running loss
        outputs = self.loss(y, outputs)
        outputs["s"] = s
        outputs["y"] = y
        # return outputs
        return outputs

    def training_step_end(self,
                          output: Dict[str, torch.Tensor]) -> None:
        self.log_dict({"train/loss": output["loss"],
                       "train/log_likelihood_of_data": -output["log_likelihood"],
                       "train/kl_divergence": output["kl_divergence"],
                       "train/cross_entropy": output["cross_entropy"],
                       "train/log_likelihood_of_targets": -output["nll"],
                       **{f"train/cross_entropy/{target_name}": output[f"cross_entropy/{target_name}"]
                          for target_name in self.categorical_targets().keys() },
                       **{f"train/log_likelihood/{target_name}": -output[f"nll/{target_name}"]
                          for target_name in self.continuous_targets().keys()
                          if f"nll/{target_name}" in output } })

    def validation_step(self,
                        batch: TernarySample,
                        batch_idx: int) -> Dict[Stage, Dict[str, torch.Tensor]]:
        """
        validation loop function called by pytorch lightning trainer

        args:
          - batch: a single batch is a binary sample, composed of data x of 
              dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch
        """
        # extract x, convert double to float
        x, y, s = batch
        # do a forward pass
        # squash along extraneous dimension
        outputs = self.forward(x.float().squeeze(1))
        # calculate each loss and sum with running loss
        outputs = self.loss(y, outputs)
        # add s and y for callbacks
        outputs["s"] = s
        outputs["y"] = y
        # return outputs
        return { Stage.validate: outputs }

    def validation_step_end(self,
                            output: Dict[Stage, Dict[str, torch.Tensor]]) -> None:
        val_output = output[Stage.validate]
        self.log_dict({"val/loss": val_output["loss"],
                       "val/log_likelihood_of_data": -val_output["log_likelihood"],
                       "val/kl_divergence": val_output["kl_divergence"],
                       "val/cross_entropy": val_output["cross_entropy"],
                       "val/log_likelihood_of_targets": -val_output["nll"],
                       **{f"val/cross_entropy/{target_name}": val_output[f"cross_entropy/{target_name}"]
                          for target_name in self.categorical_targets().keys() },
                       **{f"val/log_likelihood/{target_name}": -val_output[f"nll/{target_name}"]
                          for target_name in self.continuous_targets().keys()
                          if f"nll/{target_name}" in val_output } })

    def predict_step(self,
                     batch: TernarySample,
                     batch_idx: int) -> Dict[Stage, Dict[str, torch.Tensor]]:
        """
        test loop function called by pytorch lightning trainer

        args:
          - batch: a single batch is a binary sample, composed of data x of 
              dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch
        """
        x, y, s = batch
        # convert x to float, squash along extraneous dimension
        x = x.float().squeeze(1)
        # flatten frames and observations into same dimension
        x_flat = x.view(-1, 1, x.size(3), x.size(4))
        # encode latent representation z
        mean_flat, log_variance_flat = self.model.encoder(x_flat)
        # sample from latent distribution
        z_flat = self.model.reparameterise(mean_flat, log_variance_flat)
        # average over time frames per observation
        z = z_flat.view(x.size(0), -1, z_flat.size(1)).mean(axis=1)
        # accumulate outputs
        outputs = {"z": z.detach(),
                   "y": y.detach(),
                   "s": s }
        # compute forward pass on MLP using z
        h = self.mlp(z)
        # fetch logits
        outputs = self.logits(h, outputs)
        # return outputs
        return { Stage.test: outputs }

    def configure_optimizers(self) -> torch.optim.Optimizer:
        """
        set pytorch lightning to use the AdamW optimiser algorithm
        """
        return torch.optim.AdamW(self.parameters(),
                                 lr=self.learning_rate)
