from typing import Tuple, Dict, Callable

import numpy as np
import torch # type: ignore
from torch import nn
from torch.functional import F  # type: ignore
from torch.distributions.normal import Normal # type: ignore

from ecoacoustics.models.encoder import Encoder
from ecoacoustics.models.decoder import Decoder
from ecoacoustics.optimisers.step_kl import StepKL

def noop(x):
    return x

class VAE(nn.Module):
    @classmethod
    def build(cls,
              vggish_pretrain: bool,
              vggish_frozen: bool,
              learning_rate: float,
              latent_features: int,
              gamma: float,
              scheduler_step_size: int,
              scheduler_initial_step: int,
              negative_slope: float = 0.03) -> 'VAE':
        print("setting up Variational Autoencoder")
        encoder = Encoder(out_features=latent_features,
                          vggish_pretrain=vggish_pretrain,
                          vggish_frozen=vggish_frozen,
                          num_outputs=2)
        decoder = Decoder(in_features=latent_features,
                          out_channels=2,
                          negative_slope=negative_slope)
        step_kl = StepKL(gamma=gamma,
                         step_size=scheduler_step_size,
                         initial_step=scheduler_initial_step)
        return VAE(encoder=encoder,
                   decoder=decoder,
                   schedule=step_kl)

    def __init__(self,
                 encoder: nn.Module,
                 decoder: nn.Module,
                 schedule: Callable = noop) -> None:
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.schedule = schedule

    def forward(self,
                x: torch.Tensor) -> Dict[str, torch.Tensor]:
        """
        compute a full forward pass of the network, reduce x to latent vector z
        composed of a mean and log variance, sample from learned distributions,
        and reconstruct approximation and variance

        args:
          - x: an (n, 1, 96, 64) batch of frames of the log mel spectrogram,
              reshaped according to VGGish's dimensionality restrictons (96 x 64),
              where n equals the number of 0.96s non-overlapping frames

        returns:
          - x_hat: an approximation of x according to learned model weights
              for encoder and decoder
          - x_log_var: log scaled uncertainty of x_hat according to learned model weights
              for encoder and decoder
          - mean: a vector of mean values for learned Gaussian distributions
          - log_variance: a vector of log variance log(sigma^2) for learned
              multivariate Gaussian distribution
        """
        # flatten frames and observations into same dimension
        x_flat = x.view(-1, 1, x.size(3), x.size(4))
        # encoder model q_ϕ(z|x) to compute latent vector
        mean_flat, log_variance_flat = self.encoder(x_flat)
        # apply reparameterisation trick to sample z from learned distribution
        z_flat = self.reparameterise(mean_flat, log_variance_flat)
        # take the mean over time frames across sampled z per observation
        z = z_flat.view(x.size(0), -1, z_flat.size(1)).mean(axis=1)
        # learn to reconstruct x_hat approximation to x using decoder p_θ(x|z)
        # and learn variance per feature for uncertainty
        outputs = self.decoder(z_flat)
        x_hat_flat, x_log_var_flat = outputs.chunk(2, dim=1)
        # reshape to extract observations from frames
        x_hat = x_hat_flat.view(x.shape)
        x_log_var = x_log_var_flat.view(x.shape)
        mean = mean_flat.view(x.size(0), -1, mean_flat.size(1))
        log_variance = log_variance_flat.view(x.size(0), -1, log_variance_flat.size(1))
        # calculate the loss
        loss = self.loss(x, x_hat, x_log_var, mean, log_variance)
        # return results
        return {**loss,
                "x": x.detach(),
                "x_hat": x_hat.detach(),
                "x_log_var": x_log_var.detach(),
                "z": z }

    def loss(self,
             x: torch.Tensor,
             x_hat: torch.Tensor,
             x_log_var: torch.Tensor,
             mean: torch.Tensor,
             log_variance: torch.Tensor) -> Dict[str, torch.Tensor]:
        """
        we have a dataset of independent & identically distributed observations
        of a continuous random variable X = { x_i }_i^n, where the data has a complex,
        high dimensional, potentially non-symmetric & multi-modal underlying posterior
        distribution p(z|X) over some unobserved latent variables z. we have no
        closed form solution for calculating the true posterior, so we attempt to
        find through optimisation some approximation or surrogate posterior q(z|X)
        which is good enough to perform inference in the target domain. bayes rule
        denotes that given we have access to the prior p(z), the likelihood of the
        data p(X|z), and the marginal p(X), we can calculate the true posterior
        distribution over z given the data X. calculating the probability of the
        evidence is intractable, instead we use the KL divergence to maximise the
        lower bound on the evidence as a proxy to minimise the KL divergence between
        our surrogate posterior and the true posterior

        for a VAE, composed of a neural network encoder parameterised by weights ϕ
        and a neural network decoder parameterised by weights θ, the evidence lower
        bound (ELBO) can be written as:

          L(θ,ϕ,x_i) = -D_KL(q_ϕ(z|x_i)||p_θ(z)) + E_z~q_ϕ[log p_θ(x_i|z)]

        the ELBO minimises the KL divergence between the surrogate distribution
        and the prior on z while maximising the likelihood p(X|z)

        maximising the likelihood of the data derived from the learned latent
        distribution with samples z_i, the model learns to reconstruct the input
        data x_i to produce a variational approximation x_hat_i while learning an
        approximation q_ϕ(z|x_i) to the true posterior which produces a mapping that
        increases the accuracy of the reconstruction over all x_i

        we make a simplifying assumption that our probability distributions are gaussian
        so we can analytically determine the D_KL between the prior and the surrogate
        posterior:

            D_KL(q_ϕ(z|x_i)||p_θ(z)) = 1/2 * sum(1 + log(sigma^2) - mu^2 - sigma^2)

        to calculate the likelihood, we use a gaussian log probability to calculate
        the mean error along with a variance term to determine uncertainty and normalise
        the result as a probability distribution. this plays well with other losses
        such as cross-entropy such that we do not need to experimentally weighting terms

        we apply an annealing process to the D_KL to first prioritise the likelihood of
        the data before ramping up the scale of the D_KL to force the network to better
        approximate the true posterior. before a given parameterised threshold value, the
        multiplier for the D_KL is equal to zero, after which it asymptotically approaches 1
        scalingthe size of the D_KL to max out at its true value at each iteration

        args:
          - x: input of dimensions (n, m, 1, 96, 64)
          - x_hat: reconstructed x of dimensions (n, m, 1, 96, 64)
          - x_log_var: log variance of x of dimensions (n, m, 1, 96, 64)
          - mean: mean of latent distribution of dimensions (n, m, 1, 1, 128)
          - log_variance: log variance of latent distribution of dimensions (n, m, 1, 1, 128)

        returns:
          - reconstruction_loss: a single loss value for the batch = -D_KL + NLL
          - gaussian_nll_loss: negative log likelihood of a gaussian distribution, or gaussian log probability
          - kl_divergence: measure the divergence between approximate and true distributions
        """
        # sum the loss components, taking batchwise mean
        log_likelihood = self.__likelihood(x, x_hat, x_log_var).mean(axis=0)
        kl_divergence = self.__kl_divergence(mean, log_variance).mean(axis=0)
        loss = log_likelihood + kl_divergence
        # return a dict of loss tensors
        return {"loss": loss,
                "log_likelihood": log_likelihood,
                "kl_divergence": kl_divergence }

    def __likelihood(self,
                     x: torch.Tensor,
                     x_hat: torch.Tensor,
                     x_log_var: torch.Tensor) -> torch.Tensor:
        """
        treat input x and output x_hat as samples from gaussian distributions
        with variance x_var, calculate gaussian log probability of the data
        """
        # calculate the loss element-wise
        log_likelihood = F.gaussian_nll_loss(x_hat, x, x_log_var.exp(), reduction="none")
        # reduce along all axes except the first (supposing batch is the first dimension)
        return log_likelihood.view(log_likelihood.size(0), -1).sum(axis=1)

    def __kl_divergence(self,
                        mean: torch.Tensor,
                        log_variance: torch.Tensor) -> torch.Tensor:
        """
        sum over all values in each spectrogram and take the mean batchwise
        calculate KL divergence between prior and surrogate posterior and take the batchwise mean
        """
        # calculate the KL divergence element-wise
        kl_divergence = -1/2 * (1 + log_variance - mean.pow(2) - log_variance.exp())
        # reduce along all axes except the first
        kl_divergence = kl_divergence.view(kl_divergence.size(0), -1).sum(axis=1)
        # apply a scheduler to scale the D_KL
        # return kl_divergence
        return self.schedule(kl_divergence)

    def reparameterise(self,
                       mean: torch.Tensor,
                       log_variance: torch.Tensor) -> torch.Tensor:
        """
        given our prior on z is a standard normal distribution, use the
        reparameterisation trick to sample from the approximate posterior
        distribution to derive a variational latent vector z

        compute the standard deviation, generate some random epsilon
        sampled from our prior standard normal distribution, then
        locate and scale proportional to learned distribuion mu and sigma

        args:
          - mean: a vector of mean values for learned Gaussian distributions
          - log_variance: a vector of log variance log(sigma^2) for learned
              multivariate Gaussian distribution
        """
        # calculate standard deviation from log variance
        std_dev = (1/2 * log_variance).exp()
        # generate random samples from prior on z, a normal distribution with mean 0 and variance 1
        # update prior, return samples (z) derived from shifted normal distributions learned from data
        return Normal(mean, std_dev).rsample()
