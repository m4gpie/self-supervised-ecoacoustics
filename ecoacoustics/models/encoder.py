import copy
from typing import List

import torch # type: ignore
from torch import nn

from ecoacoustics.models.vggish import VGGish

"""
probabilistic encoder q_ϕ(z|x) maps input x (16kHz log-scaled mel-frequency spectrograms)
to a corresponding normal distribution N(mu, sigma) in the latent space. our convolutional
encoder uses the VGGish CNN, and first two layers in the embeddings, with the final layer
removed and two new output layers added to output a mean and log variance
"""

class Encoder(nn.Module):
    def __init__(self,
                 vggish_pretrain: bool = False,
                 vggish_frozen: bool = False,
                 out_features: int = 128,
                 negative_slope: float = 0.03,
                 num_outputs: int = 2):
        super().__init__()
        # import vggish with pre-loaded weights
        self.vggish = VGGish(pretrain=vggish_pretrain,
                             frozen=vggish_frozen)
        # remove the final layer
        self.vggish.embeddings = nn.Sequential(*[self.vggish.embeddings[i] for i in range(4)])
        # create n output layers based on the number of output channels
        # shape = (n, 1, 96, 64)
        # self.features = nn.Sequential(nn.Conv2d(1, 64, kernel_size=3, padding=1),
        #                               nn.GroupNorm(num_groups=1, num_channels=64),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 64, 96, 64)
        #                               nn.Conv2d(64, 64, kernel_size=1, stride=2),
        #                               nn.GroupNorm(num_groups=1, num_channels=64),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 64, 48, 32)
        #                               nn.Conv2d(64, 128, kernel_size=3, padding=1),
        #                               nn.GroupNorm(num_groups=1, num_channels=128),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 128, 48, 32)
        #                               nn.Conv2d(128, 128, kernel_size=1, stride=2),
        #                               nn.GroupNorm(num_groups=1, num_channels=128),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 128, 24, 16)
        #                               nn.Conv2d(128, 256, kernel_size=3, padding=1),
        #                               nn.GroupNorm(num_groups=1, num_channels=256),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 256, 24, 16)
        #                               nn.Conv2d(256, 256, kernel_size=3, padding=1),
        #                               nn.GroupNorm(num_groups=1, num_channels=256),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 256, 24, 16)
        #                               nn.Conv2d(256, 256, kernel_size=1, stride=2),
        #                               nn.GroupNorm(num_groups=1, num_channels=256),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 256, 12, 8)
        #                               nn.Conv2d(256, 512, kernel_size=3, padding=1),
        #                               nn.GroupNorm(num_groups=1, num_channels=512),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 512, 12, 8)
        #                               nn.Conv2d(512, 512, kernel_size=3, padding=1),
        #                               nn.GroupNorm(num_groups=1, num_channels=512),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                               # shape = (n, 512, 12, 8)
        #                               nn.Conv2d(512, 512, kernel_size=1, stride=2),
        #                               nn.GroupNorm(num_groups=1, num_channels=512),
        #                               nn.LeakyReLU(negative_slope=negative_slope, inplace=True))
        # # # shape = (n, 512 * 6 * 4, 1, 1)
        # self.embeddings = nn.Sequential(nn.Linear(in_features=512 * 4 * 6, out_features=4096),
        #                                 nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
        #                                 # shape = (n, 4096, 1, 1)
        #                                 nn.Linear(in_features=4096, out_features=4096),
        #                                 nn.LeakyReLU(negative_slope=negative_slope, inplace=True))
        # shape = (n, 4096, 1, 1)
        self.outputs = nn.ModuleList([nn.Linear(in_features=4096, out_features=out_features)
                                      for i in range(num_outputs)])

    def forward(self,
                x: torch.Tensor) -> List[torch.Tensor]:
        """
        perform a forward pass to encoder a latent feature embedding
        """
        # run VGGish CNN for feature detection and embeddings minus final layer
        x = self.vggish(x)
        # compute outputs
        return [output(x) for output in self.outputs]

    def flatten(self,
                x: torch.Tensor) -> torch.Tensor:
        x = torch.transpose(x, 1, 3)
        x = torch.transpose(x, 1, 2)
        x = x.contiguous()
        return x.view(x.size(0), -1)
