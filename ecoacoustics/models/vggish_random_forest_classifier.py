from typing import Dict, Tuple, List, Union, Any
from numpy import typing as npt

import torch # type: ignore
import numpy as np
import numpy.typing as npt
import pytorch_lightning as pl # type: ignore

from conduit.data import TernarySample # type: ignore
from ecoacoustics.types import Stage
from ecoacoustics.models.vggish import VGGish

class VGGishRandomForestClassifier(pl.LightningModule):
    def __init__(self) -> None:
        """
        args:
          - latent_features: number of features in the latent space
        """
        super().__init__()
        # load the model
        self.encoder = VGGish(pretrain=True)

    def forward(self,
                x: torch.Tensor) -> torch.Tensor:
        """
        a full forward pass of VGGish for the batch

        args:
          - x: a full batch of data x of dimensions (n, m, 1 96, 64), where n = batch_size,
              m = number of frames, 1 = number of channels, 96 = time axis, 64 = frequency axis,
              i.e. number of mel frequency bins

        returns:
          - z: a full batch of sampled latent variables (n, m, 128)
        """
        # merge frames and batch to process concurrently
        # x_flat = [n x m, 1, 96, 64]
        x_flat = x.view(-1, 1, x.size(3), x.size(4))
        # perform forward pass on the batch, encode flattened
        # latent vectors z then reconstruct with decoder
        z_flat = self.encoder.forward(x_flat)
        # unflatten z,  z_flat = [n x m, 128]
        # to [n, m, 128] to separate batches from frames
        z = z_flat.view(x.size(0), -1, z_flat.size(1))
        # average z over time
        z_avg = z.mean(axis=1)
        # return results
        return z_avg

    def predict_step(self,
                     batch: Dict[Stage, TernarySample],
                     batch_idx: int) -> Dict[Stage, Dict[str, npt.NDArray]]:
        """
        testing loop function, performs a forward pass on both training and
        testing data. latent dimension z is computed along with target y
        and returned for handling in classification task at epoch end

        args:
          - batch: a single batch contains a batch of training data, and
              a batch of testing data, each is a binary sample, composed
              of data x of dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch

        returns:
          - output: a dictionary of dictionaries, keeping training and testing
              logits and targets separate
        """
        output = {}
        # for training and testing data
        for stage, sub_batch in batch.items():
            x, y, s = sub_batch
            # encode latent representation z
            z = self.forward(x.float().squeeze(1))
            # return logits and targets
            output[stage] = {"z": z.detach(),
                             "y": y.detach(),
                             "s": s }
        return output
