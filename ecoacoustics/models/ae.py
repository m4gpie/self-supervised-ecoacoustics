from typing import Dict
import numpy.typing as npt

import torch # type: ignore
import numpy as np
import pytorch_lightning as pl # type: ignore

from torch.functional import F  # type: ignore
from conduit.data import TernarySample # type: ignore
from ecoacoustics.models.encoder import Encoder
from ecoacoustics.models.decoder import Decoder

class AutoEncoder(pl.LightningModule):
    @classmethod
    def build(cls,
              vggish_pretrain: bool,
              vggish_frozen: bool,
              learning_rate: float,
              latent_features: int) -> 'AutoEncoder':
        print("setting up Autoencoder")
        encoder = Encoder(out_features=latent_features,
                          vggish_pretrain=vggish_pretrain,
                          vggish_frozen=vggish_frozen,
                          num_outputs=1)
        decoder = Decoder(in_features=latent_features,
                          out_channels=1,
                          negative_slope=0.03)
        return AutoEncoder(encoder=encoder,
                           decoder=decoder)

    def __init__(self, encoder, decoder) -> None:
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self,
                x: torch.Tensor) -> Dict[str, torch.Tensor]:
        # flatten frames and observations into same dimension
        x_flat = x.view(-1, 1, x.size(3), x.size(4))
        z_flat, = self.encoder(x_flat)
        # reshape to extract observations from frames
        x_hat_flat = self.decoder(z_flat)
        x_hat = x_hat_flat.view(x.shape)
        z = z_flat.view(x.size(0), -1, z_flat.size(1))
        # take the mean over frames a.k.a. time hops
        z = z.mean(axis=1)
        # calculate the loss
        loss = self.loss(x, x_hat)
        # return results
        return {**loss,
                "x": x.detach(),
                "x_hat": x_hat.detach(),
                "z": z.detach() }

    def loss(self,
             x: torch.Tensor,
             x_hat: torch.Tensor) -> torch.Tensor:
        # return the batchwise mean of the loss calculated element-wise
        # and summed over each spectrogram
        loss = F.mse_loss(x_hat, x, reduction="none")
        loss = loss.view(loss.size(0), -1).mean(axis=1).mean(axis=0)
        return {"loss": loss }

    def training_step(self,
                      batch: TernarySample,
                      batch_idx: int) -> Dict[str, torch.Tensor]:
        """
        training loop function called by pytorch lightning trainer

        args:
          - batch: a single batch is a binary sample, composed of data x of
              dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch

        returns:
          - loss: a dict of the loss broken down
          - z: a tensor of smoothed out latent representations over time
          - y: the corresponding labels for the batch
        """
        # extract x, convert double to float
        # and squash along extraneous dimension
        x, y, s = batch
        # do a forward pass
        outputs = self.forward(x.float().squeeze(1))
        # return outputs
        outputs["s"] = s
        outputs["y"] = y
        return outputs

    def training_step_end(self,
                          output: Dict[str, torch.Tensor]) -> None:
        self.log_dict({"train/loss": output["loss"] })
