from typing import Dict, Tuple, List, Union, Any
from numpy import typing as npt

import torch # type: ignore
import numpy as np
import numpy.typing as npt
import pytorch_lightning as pl # type: ignore

from conduit.data import TernarySample # type: ignore
from ecoacoustics.models.vae import VAE
from ecoacoustics.types import Stage

class VAERandomForestClassifier(pl.LightningModule):
    def __init__(self,
                 vggish_pretrain: bool,
                 vggish_frozen: bool,
                 learning_rate: float,
                 latent_features: int,
                 gamma: float,
                 scheduler_step_size: int,
                 scheduler_initial_step: int) -> None:
        """
        args:
          - learning_rate: rate of learning used in optimiser
          - latent_features: number of features in the latent space
        """
        super().__init__()
        self.learning_rate = learning_rate
        self.save_hyperparameters()
        # initialize the model
        self.model = VAE.build(vggish_pretrain=vggish_pretrain,
                               vggish_frozen=vggish_frozen,
                               learning_rate=learning_rate,
                               latent_features=latent_features,
                               gamma=gamma,
                               scheduler_step_size=scheduler_step_size,
                               scheduler_initial_step=scheduler_initial_step)

    def training_step(self,
                      batch: TernarySample,
                      batch_idx: int) -> Dict[str, torch.Tensor]:
        """
        training loop function called by pytorch lightning trainer

        args:
          - batch: a single batch is a binary sample, composed of data x of
              dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch

        returns:
          - loss: a dict of the loss broken down
          - z: a tensor of smoothed out latent representations over time
          - y: the corresponding labels for the batch
        """
        # extract x, convert double to float
        # and squash along extraneous dimension
        x, y, s = batch
        # do a forward pass
        outputs = self.model(x.float().squeeze(1))
        # return outputs
        outputs["s"] = s
        outputs["y"] = y
        return outputs

    def training_step_end(self,
                          output: Dict[str, torch.Tensor]) -> None:
        self.log_dict({"train/loss": output["loss"],
                       "train/log_likelihood_of_data": -output["log_likelihood"],
                       "train/kl_divergence": output["kl_divergence"] })

    def validation_step(self,
                        batch: Dict[Stage, TernarySample],
                        batch_idx: int) -> Dict[Stage, Dict[str, Union[torch.Tensor, npt.NDArray]]]:
        """
        validation loop function, performs a forward pass on both training and
        validation data. latent dimension z is extracted along with target y
        and returned for handling in classification task at epoch end

        args:
          - batch: a single batch contains a batch of training data, and
              a batch of validation data, each is a binary sample, composed
              of data x of dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch

        returns:
          - output: a dictionary of dictionaries, keeping training and validation
              logits and targets separate
        """
        output = {}
        for stage, sub_batch in batch.items():
            # extract x, convert double to float
            # and squash along extraneous dimension
            x, y, s = sub_batch
            x = x.float().squeeze(1)
            # do a forward pass
            outputs = self.model(x)
            # accumulate outputs
            outputs["s"] = s
            outputs["y"] = y
            output[stage] = outputs
        return output

    def validation_step_end(self,
                            outputs: Dict[Stage, Dict[str, torch.Tensor]]) -> None:
        val_outputs = outputs[Stage.validate]
        self.log_dict({"val/loss": val_outputs["loss"],
                       "val/log_likelihood_of_data": -val_outputs["log_likelihood"],
                       "val/kl_divergence": val_outputs["kl_divergence"] })

    def predict_step(self,
                     batch: Dict[Stage, TernarySample],
                     batch_idx: int) -> Dict[Stage, Dict[str, npt.NDArray]]:
        """
        performs a forward pass on both training and test data
        latent dimension z is extracted along with target y
        and returned for handling in classification task at epoch end

        args:
          - batch: a single batch contains a batch of training data, and
              a batch of testing data, each is a binary sample, composed
              of data x of dimensions (n, 1, 96, 64) plus target labels y
          - batch_idx: the index of the batch

        returns:
          - output: a dictionary of dictionaries, keeping training and testing
              logits and targets separate
        """
        outputs = {}
        # for training and testing data
        for stage, sub_batch in batch.items():
            x, y, s = sub_batch
            x = x.float().squeeze(1)
            # flatten frames and observations into same dimension
            x_flat = x.view(-1, 1, x.size(3), x.size(4))
            # encode latent representation z
            mean_flat, log_variance_flat = self.model.encoder(x_flat)
            # sample from latent distribution
            z_flat = self.model.reparameterise(mean_flat, log_variance_flat)
            # average over time frames per observation
            z = z_flat.view(x.size(0), -1, z_flat.size(1)).mean(axis=1)
            # accumulate outputs
            output = {"z": z.detach(),
                      "y": y.detach(),
                      "s": s }
            outputs[stage] = output
        return outputs

    def configure_optimizers(self) -> torch.optim.Optimizer:
        """
        set pytorch lightning to use the AdamW optimiser algorithm
        """
        return torch.optim.AdamW(self.parameters(),
                                 lr=self.learning_rate)
