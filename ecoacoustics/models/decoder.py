from typing import Tuple
from collections import OrderedDict

import torch # type: ignore
from torch import nn

class Decoder(nn.Module):
    """
    probabilistic decoder p_θ(x|z) maps a latent space sample z back into input data space

    our convolutional decoder uses a mirrored architecture to the encoder, based on the VGGish CNN
    taking 128 input samples z and mapping these to 16kHz log-scaled Mel-frequency spectrograms
    """
    def __init__(self,
                 in_features: int = 128,
                 out_channels: int = 2,
                 negative_slope: float = 0.03):
        super().__init__()
        # decoder fully connected network using single hidden layer,
        # receives sampled latent features and reduces to 4096 latent vector
        self.embedding = nn.Sequential(nn.Linear(in_features=in_features, out_features=4096),
                                       nn.ReLU(inplace=True),
                                       nn.Linear(in_features=4096, out_features=4096),
                                       nn.ReLU(inplace=True),
                                       nn.Linear(in_features=4096, out_features=512 * 4 * 6),
                                       nn.ReLU(inplace=True)) # output (n, 512, 6, 4)
        # reshape to input correct dimensions to convolutional feature reconstruction
        # convolutions applied in reverse to encoder with learned upsampling
        # shape = (n, 512, 6, 4)
        self.features = nn.Sequential(nn.ConvTranspose2d(512, 512, kernel_size=2, stride=2),
                                      nn.GroupNorm(num_groups=1, num_channels=512),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 512, 12, 8)
                                      nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
                                      nn.GroupNorm(num_groups=1, num_channels=512),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 512, 12, 8)
                                      nn.Conv2d(512, 256, kernel_size=3, stride=1, padding=1),
                                      nn.GroupNorm(num_groups=1, num_channels=256),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 256, 12, 8)
                                      nn.ConvTranspose2d(256, 256, kernel_size=2, stride=2),
                                      nn.GroupNorm(num_groups=1, num_channels=256),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 256, 24, 16)
                                      nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
                                      nn.GroupNorm(num_groups=1, num_channels=256),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 256, 24, 16)
                                      nn.Conv2d(256, 128, kernel_size=3, stride=1, padding=1),
                                      nn.GroupNorm(num_groups=1, num_channels=128),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 256, 12, 8)
                                      nn.ConvTranspose2d(128, 128, kernel_size=2, stride=2),
                                      nn.GroupNorm(num_groups=1, num_channels=128),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 128, 48, 32)
                                      nn.Conv2d(128, 64, kernel_size=3, stride=1, padding=1),
                                      nn.GroupNorm(num_groups=1, num_channels=64),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 64, 48, 32)
                                      nn.ConvTranspose2d(64, 64, kernel_size=2, stride=2),
                                      nn.GroupNorm(num_groups=1, num_channels=64),
                                      nn.LeakyReLU(negative_slope=negative_slope, inplace=True),
                                      # shape = (n, 64, 96, 64)
                                      nn.Conv2d(64, out_channels, kernel_size=3, stride=1, padding=1))
        # shape = (n, m, 96, 64)

    def unflatten(self,
                  x: torch.Tensor) -> torch.Tensor:
        """
        unflatten embedding to apply convolution

        args:
          - x: input of shape (N, 12288)

        returns:
          - x: output of shape (N, 512, 6, 4)
        """
        return x.view(x.size(0), 6, 4, 512).permute(0, 3, 1, 2)

    def forward(self,
                x: torch.Tensor) -> Tuple[torch.Tensor,
                                          torch.Tensor]:
        """
        full forward pass of decoder, from final latent layer,
        compute embedding, unflatten, then apply convolutions
        to output two channels, a reconstruction & its uncertainty
        """
        x = self.embedding(x)
        x = self.unflatten(x)
        x = self.features(x)
        return x
