import torch  # type: ignore

class MinMaxScaler(torch.nn.Module):
    """
    given we know the maximum and minimum values of a given dataset, in our case
    this would be the values of greatest intensity across all spectrograms,
    we normalize each spectrogram according to these values
    """
    def __init__(self,
                 x_max: torch.Tensor,
                 x_min: torch.Tensor):
        super().__init__()
        self.x_max = x_max
        self.x_min = x_min

    def forward(self,
                x: torch.Tensor) -> torch.Tensor:
        return (x - self.x_min) / (self.x_max - self.x_min)

    def backward(self,
                 x: torch.Tensor) -> torch.Tensor:
        return self.locate(self.scale(x))

    def scale(self,
              x: torch.Tensor) -> torch.Tensor:
        return x * (self.x_max - self.x_min)

    def locate(self,
               x: torch.Tensor) -> torch.Tensor:
        return x + self.x_min
