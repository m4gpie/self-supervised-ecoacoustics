from typing import Union
from numpy import typing as npt

import torch  # type: ignore

class StandardScaler(torch.nn.Module):
    """
    given we know the mean and standard deviation values of a given dataset
    scale the values of x around a mean of zero and standard deviation of 1
    """
    def __init__(self,
                 mean: Union[float, npt.NDArray, torch.Tensor],
                 std: Union[float, npt.NDArray, torch.Tensor]):
        super().__init__()
        self.mean = mean
        self.std = std

    def forward(self,
                x: torch.Tensor) -> torch.Tensor:
        return (x - self.mean) / self.std

    def backward(self,
                 x: torch.Tensor) -> torch.Tensor:
        return self.locate(self.scale(x))

    def scale(self,
              x: torch.Tensor) -> torch.Tensor:
        return x * self.std

    def locate(self,
               x: torch.Tensor) -> torch.Tensor:
        return x + self.mean
