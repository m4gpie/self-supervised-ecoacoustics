import torch # type: ignore
import numpy as np

class Frame(torch.nn.Module):
    def __init__(self,
                 stft_hop_length_seconds: float = 0.010,
                 window_seconds: float = 0.96,
                 hop_seconds: float = 0.96):
        """
        reframe data along an additional axis according to the sampling rate,
        number of seconds in a window and the number of seconds in a hop.

        default to 96 x 10ms non-overlapping frames with no gaps inbetween

        args:
          - stft_hop_length_seconds: the length in seconds of the short time Fourier transform
              to determine the sampling rate of our features
          - window_seconds: the window length of a given example
          - hop_seconds: the hop length between examples, such that we can either overlap or
              skip seconds of the data
        """
        super().__init__()
        self.stft_hop_length_seconds = stft_hop_length_seconds
        self.window_seconds = window_seconds
        self.hop_seconds = hop_seconds
        self.features_sample_rate = 1 / stft_hop_length_seconds
        self.window_length = int(round(self.window_seconds * self.features_sample_rate))
        self.hop_length = int(round(self.hop_seconds * self.features_sample_rate))

    def forward(self,
                data: torch.Tensor) -> torch.Tensor:
        """
        convert array into a sequence of successive possibly overlapping frames

        an nD array of shape (num_samples, ...) is converted into an (n+1)D array
        of shape (num_frames, window_length, ...), where each frame starts hop_length
        points after the preceding one

        this is accomplished using stride_tricks, so the original data is not
        copied. However, there is no zero-padding, so any incomplete frames at the
        end are not included

        args:
          - data: torch.Tensor of dimension (n >= 1)

        returns:
          - (n+1)D torch.Tensor where first dimension of data is reframed along an additional axis
        """
        # transpose so time is on the vertical axes instead of frequency bins
        X = data.transpose(-2, -1).numpy()
        # fetch the number of samples on the time axis
        num_samples = X.shape[0]
        # calculate the number of frames
        num_frames = 1 + ((num_samples - self.window_length) // self.hop_length)
        # define shape to add an extra dimension, picking from our dataset
        # using the window length and num frames to stride across the data
        shape = (num_frames, self.window_length) + X.shape[1:]
        # strides = (bytes per row, bytes per column) w remaining dimensions
        strides = (X.strides[0] * self.hop_length,) + X.strides
        # add extra dimension to the data
        reframed = np.lib.stride_tricks.as_strided(X,
                                                   shape=shape,
                                                   strides=strides)
        # reframe the data along an additional axis and unsqueeze along frame dimension
        # to return as a single data point for batching
        return torch.as_tensor(reframed, device=data.device).unsqueeze(1).unsqueeze(0)

    def backward(self,
                 data: torch.Tensor) -> torch.Tensor:
        """
        squash frames back into sequence

        an nD array of shape (num_frames, window_length, ...) is converted into an (n-1)D
        array of shape (num_samples, ...). without the original number of samples, we cannot
        reverse calculate the correct value, so this will only work when the forward
        function computed non-overlappging frames, otherwise duplicate values will appear

        this is accomplished using stride_tricks, so the original data is not
        copied

        args:
          - data: torch.Tensor of dimension (n >= 2)

        returns:
          - (n-1)D torch.Tensor where first dimension of data is squashed into the next
        """
        # remove extraneous dimensions, and convert to numpy
        X = data.squeeze(0).squeeze(1).numpy()
        # extract number of frames and window length
        num_frames, window_length, *_ = X.shape
        # compute the number of samples
        num_samples = num_frames * window_length
        # define shape to remove the dimension
        shape = X.shape[2:] + (num_samples,)
        # define strides for new dimensions
        strides = X.strides[2:] + (int(X.strides[0] / self.hop_length),)
        # remove dimension of data
        unframed = np.lib.stride_tricks.as_strided(X,
                                                   shape=shape,
                                                   strides=strides)
        # return as a tensor
        return torch.as_tensor(unframed, device=data.device)
