from typing import Union
from numpy import typing as npt

import librosa  # type: ignore
import torch # type: ignore
import numpy as np

"""
The audio is divided into non-overlapping 960 ms frames. This
gave approximately 20 billion examples from the 70M videos. Each
frame inherits all the labels of its parent video. The 960 ms frames
are decomposed with a short-time Fourier transform applying 25 ms
windows every 10 ms. The resulting spectrogram is integrated into
64 mel-spaced frequency bins, and the magnitude of each bin is log-
transformed after adding a small offset to avoid numerical issues.
This gives log-mel spectrogram patches of 96 × 64 bins that form
the input to all classifiers.

https://arxiv.org/pdf/1609.09430.pdf
"""

class LogMelSpectrogram(torch.nn.Module):
    def __init__(self,
                 sample_rate: int = 16_000,
                 stft_window_length_seconds: float = 0.025,
                 stft_hop_length_seconds: float = 0.010,
                 log_offset: float = 1e-3,
                 num_mel_bins: int = 64,
                 mel_min_hz: float = 125,
                 mel_max_hz: float = 7500,
                 mel_break_freq_hertz: float = 700.0,
                 mel_high_freq_q: float = 1127.0) -> None:
        """
        convert waveform to a mel-scale spectrogram

        reference code:
        - https://github.com/wearepal/ecoacoustics/blob/pytorch-lightning/ecoacoustics/transforms/logmelspectrogram.py
        - https://github.com/tensorflow/models/blob/4079c5d9693142a406f6ff392d14e2034b5f496d/research/audioset/vggish/mel_features.py

        args:
          - audio_sample_rate: samples per second of the audio at the input to the spectrogram,
              to figure out the actual frequencies for each spectrogram bin, dictating how
              they are mapped into mel space
          - log_offset: add to prevent -inf for zero values when logarithmic scale applied
          - stft_window_length_seconds: duration of each window to analyze
          - stft_hop_length_seconds: advance between successive analysis windows
          - num_mel_bins: how many bands in the resulting mel spectrum (corresponds to the number
              of columns in the output mel spectrogram matrix
          - mel_min_hz: lower bound on the frequencies to be included in the mel spectrum,
              corresponding to the lower edge of the lowest triangular band
          - mel_max_hz: the desired top edge of the highest frequency band
        """
        super().__init__()
        self.window_length = round(sample_rate * stft_window_length_seconds)
        self.hop_length = round(sample_rate * stft_hop_length_seconds)
        self.fft_length = 2 ** int(np.ceil(np.log(self.window_length) / np.log(2.0)))
        self.log_offset = log_offset
        self.sample_rate = sample_rate
        self.num_mel_bins = num_mel_bins
        self.mel_min_hz = mel_min_hz
        self.mel_max_hz = mel_max_hz
        self.mel_break_freq_hertz = mel_break_freq_hertz
        self.mel_high_freq_q = mel_high_freq_q

    def forward(self,
                data: torch.tensor) -> torch.tensor:
        """
        compute the mel spectrogram with specified parameters,
        decomposes the signal into fft bins using an stft before applying a
        mel transformation matrix to project fft bins to mel-frequency bins
        """
        signal = data.numpy().squeeze(0)
        # compute the STFT
        fft = librosa.stft(signal,
                           hop_length=self.hop_length,
                           win_length=self.window_length,
                           n_fft=self.fft_length,
                           window='hann')
        # extract magnitude spectrogram
        spectrogram = np.abs(fft)
        # compute the mel using provided bounds
        spectrogram = librosa.feature.melspectrogram(S=spectrogram,
                                                     sr=self.sample_rate,
                                                     n_mels=self.num_mel_bins,
                                                     fmin=self.mel_min_hz,
                                                     fmax=self.mel_max_hz)
        # move back to tensor
        spectrogram = torch.as_tensor(spectrogram,
                                      device=data.device)
        # apply the log with offset to prevent log of zero
        spectrogram = torch.log(spectrogram + self.log_offset)
        # return transpose so freq bins are on the y axis
        return spectrogram

    def backward(self,
                 data: torch.Tensor) -> torch.Tensor:
         # invert the log and convert to numpy
         spectrogram = data.exp().numpy()
         # spectrogram = data.numpy()
         # reverse the mel
         inv_fft = librosa.feature.inverse.mel_to_stft(spectrogram,
                                                       sr=self.sample_rate,
                                                       n_fft=self.fft_length)
         # estimate the phase spectrogram and the original signal
         inv_signal = librosa.griffinlim(inv_fft,
                                         hop_length=self.hop_length,
                                         win_length=self.window_length,
                                         n_fft=self.fft_length,
                                         window='hann')
         # convert to tensor
         inv_signal = torch.as_tensor(inv_signal,
                                      device=data.device)
         return inv_signal.unsqueeze(0)
