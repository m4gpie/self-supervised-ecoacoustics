import torch  # type: ignore

class StepKL(torch.nn.Module):
    def __init__(self,
                 gamma: float,
                 step_size: int,
                 initial_step: int) -> None:
        """
        linearly scale the KL proportional to a value incrementing between zero and unity
        when scale has maxed out at 1, we don't want to increase it any more, we scale on
        each step size when scale is less than or equal to 1 and the step is non-zero

        args:
            - step_size: the number of steps required to increment the scale factor by gamma
            - gamma: value greater than 0 and less than 1 by which we increase the scale factor until unity
        """
        assert 0.0 < gamma <= 1.0, 'gamma must be between 0 and 1'
        super().__init__()
        self.gamma = gamma
        self.step_size = step_size
        self.initial_step = initial_step
        self.step = 0
        self.scale = 0.0

    def forward(self,
                x: torch.Tensor) -> torch.Tensor:
        if ((0.0 <= self.gamma < 1.0) and
                (self.step >= self.initial_step) and
                (self.step % self.step_size == 0)):
            self.scale += self.gamma
        self.step += 1
        return self.scale * x
