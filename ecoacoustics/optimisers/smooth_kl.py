import torch  # type: ignore

class SmoothKL(torch.nn.Module):
    def __init__(self,
                 delta: float = 1.0,
                 max_x_interval: float = 1000.0) -> None:
        super().__init__()
        self.max_x_interval = max_x_interval
        self.delta = delta
        self.step = 0

    def forward(self,
                x: torch.Tensor) -> torch.Tensor:
        return x * torch.sigmoid((self.delta * ((2 * self.step) - self.max_x_interval)) / self.max_x_interval)
