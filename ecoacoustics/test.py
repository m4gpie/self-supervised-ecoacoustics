import click
import torch  # type: ignore
import wandb  # type: ignore
import pytorch_lightning as pl # type: ignore
from pathlib import Path

from ecoacoustics.utils import load_targets, load_hyperparameters, preprocess
from ecoacoustics.utils import get_environment_mode, is_development
from ecoacoustics.callbacks.z_umap import ZUMAP
from ecoacoustics.callbacks.reconstruct import Reconstructor
from ecoacoustics.callbacks.random_forest import RandomForest
from ecoacoustics.callbacks.mlp_metrics import MLPMetrics
from ecoacoustics.callbacks.class_activation_mapper import ClassActivationMapper
from ecoacoustics.models.vae_random_forest_classifier import VAERandomForestClassifier
from ecoacoustics.models.vae_mlp_classifier import VAEMLPClassifier
from ecoacoustics.models.vggish_random_forest_classifier import VGGishRandomForestClassifier

@click.command()
@click.option('--seed', type=int, default=42, help="random seed")
@click.option('--checkpoint-path', '-m', required=True, type=str, help="path to pytorch checkpoint for trained VAE")
def vae_random_forest_classifier(seed,
                                 checkpoint_path):
    # setting seed
    pl.seed_everything(seed)
    # load data hyperparams
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    options.update({"seed": seed })
    # load target info
    targets, test_targets = load_targets(root_path)
    # create data splits and transforms
    data_module, label_decoder, frame_transform = preprocess(root_path, seed, targets, options)
    # load checkpoint
    print(f"setting up VAE from checkpoint {checkpoint_path}")
    model = VAERandomForestClassifier.load_from_checkpoint(checkpoint_path)
    # compute a random forest on the latent space
    random_forest = RandomForest(targets=test_targets,
                                 decoder=label_decoder,
                                 model_name='VAE',
                                 num_estimators=options["num_estimators"])
    # umap on the latent space
    zumap = ZUMAP(targets=test_targets,
                  decoder=label_decoder,
                  save_path=(root_path / 'ecoacoustics' / 'data'),
                  seed=seed,
                  model_name="VAE Random Forest Classifier")
    # setup trainer options
    env = get_environment_mode()
    train_options = options[env]
    # update the trainer parameters
    train_options.update({"accelerator": "auto",
                          "devices": "auto",
                          "callbacks":[zumap, random_forest],
                          "logger": pl.loggers.WandbLogger(project="variational-autoencoder-ecoacoustics"),
                          "fast_dev_run": is_development() })
    # setup a trainer
    trainer = pl.Trainer(**train_options)
    # upload model to W&B
    wandb.save(checkpoint_path)
    # update remote config with all hyperparameters
    wandb.config.update(options)
    # predict the test data
    trainer.predict(model, dataloaders=data_module.test_dataloader(combined=True))

@click.command()
@click.option('--seed', type=int, default=42, help="random seed")
@click.option('--checkpoint-path', '-m', required=True, type=str, help="path to pytorch checkpoint for trained VAE & MLP")
def vae_mlp_classifier(seed,
                       checkpoint_path):
    pl.seed_everything(seed)
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    options.update({"seed": seed })
    targets, test_targets = load_targets(root_path)
    data_module, label_decoder, frame_transform = preprocess(root_path, seed, targets, options)
    # load checkpoint
    print(f"setting up VAE from checkpoint {checkpoint_path}")
    model = VAEMLPClassifier.load_from_checkpoint(checkpoint_path)
    # generate MLP metrics
    mlp_metrics = MLPMetrics(targets=test_targets,
                             decoder=label_decoder)
    # umap on the latent space
    zumap = ZUMAP(targets=test_targets,
                  decoder=label_decoder,
                  save_path=(root_path / 'ecoacoustics' / 'data'),
                  seed=seed,
                  model_name="VAE MLP Classifier")
    # setup trainer options
    env = get_environment_mode()
    train_options = options[env]
    # update the trainer parameters
    train_options.update({"accelerator": "auto",
                          "devices": "auto",
                          "callbacks":[zumap, mlp_metrics],
                          "logger": pl.loggers.WandbLogger(project="variational-autoencoder-ecoacoustics"),
                          "fast_dev_run": is_development() })
    # setup a trainer
    trainer = pl.Trainer(**train_options)
    # upload model to W&B
    wandb.save(checkpoint_path)
    # update remote config with all hyperparameters
    wandb.config.update(options)
    # predict the test data
    trainer.predict(model, dataloaders=data_module.test_dataloader(combined=False))

@click.command()
@click.option('--seed', type=int, default=42, help="random seed")
def vggish_random_forest_classifier(seed):
    """
    evaluate VGGish on the ecoacoustic dataset and use time-averaged sampled
    latent vector z on a random forest task to estimate target variables
    """
    pl.seed_everything(seed)
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    options.update({"seed": seed })
    targets, test_targets = load_targets(root_path)
    data_module, label_decoder, frame_transform = preprocess(root_path, seed, targets, options)
    # define our model
    model = VGGishRandomForestClassifier()
    # define callbacks
    zumap = ZUMAP(targets=test_targets,
                  decoder=label_decoder,
                  seed=seed,
                  model_name="VGGish Random Forest Classifier")
    random_forest = RandomForest(targets=test_targets,
                                 decoder=label_decoder,
                                 model_name='VGGish',
                                 num_estimators=options["num_estimators"])
    # TODO:
    # class_activation_mapper = ClassActivationMapper(targets=targets,
    #                                                 frame_transform=frame_transform,
    #                                                 decoder=label_decoder,
    #                                                 sample_rate=options["sample_rate"],
    #                                                 stft_hop_length_seconds=options["stft_hop_length_seconds"])
    # setup trainer options
    env = get_environment_mode()
    train_options = options[env]
    # update the trainer parameters
    train_options.update({"accelerator": "auto",
                          "devices": "auto",
                          "callbacks":[zumap, random_forest],
                          "logger": pl.loggers.WandbLogger(project="variational-autoencoder-ecoacoustics"),
                          "fast_dev_run": is_development() })
    # setup a trainer
    trainer = pl.Trainer(**train_options)
    # update remote config with all hyperparameters
    wandb.config.update(options)
    # evaluate the model
    trainer.predict(model, dataloaders=data_module.test_dataloader(combined=True))

@click.group()
def test():
    pass

test.add_command(vae_random_forest_classifier)
test.add_command(vae_mlp_classifier)
test.add_command(vggish_random_forest_classifier)

if __name__ == '__main__':
    test()
