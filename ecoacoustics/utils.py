from typing import Any, Dict, Tuple, Union
from numpy import typing as npt

import os
import re
import yaml
import torch # type: ignore
import numpy as np
from pathlib import Path
from enum import Enum, auto
from matplotlib.colors import Colormap  # type: ignore

from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.data.data_module import EcoacousticsDataModuleSplitBySite

class Environment(Enum):
    production = auto()
    development = auto()

class TargetsMixin:
    def categorical_targets(self):
        return {target_name: target_attrs for target_name, target_attrs in self.targets.items()
                if target_attrs["type"] == 'categorical' }

    def continuous_targets(self):
        return  {target_name: target_attrs for target_name, target_attrs in self.targets.items()
                 if target_attrs["type"] == 'continuous' }

def load_targets(root_path: Path) -> Tuple[Dict[str, Any], Dict[str, Any]]:
    # load target info
    with open(root_path / 'config' / 'targets.yml') as file:
        all_targets = yaml.load(file, Loader=yaml.Loader)
    # remove site
    test_targets = {target_name: target_attrs
                    for target_name, target_attrs in all_targets.items()
                    if target_name != 'site'}
    return all_targets, test_targets

def load_hyperparameters(root_path) -> Dict[str, Any]:
    # load parameters
    with open(root_path / 'config' / 'train_params.yml') as file:
        options = yaml.load(file, Loader=yaml.Loader)
    return options

def preprocess(root_path: Path,
               seed: int,
               targets: Dict[str, Any],
               options: Dict[str, Any]) -> Tuple:
    # create log mel spectrogram transform with default hyper-parameters
    spectrogram_transform = LogMelSpectrogram(sample_rate=options["sample_rate"],
                                              stft_window_length_seconds=options["stft_window_length_seconds"],
                                              stft_hop_length_seconds=options["stft_hop_length_seconds"],
                                              log_offset=options["log_offset"],
                                              num_mel_bins=options["num_mel_bins"],
                                              mel_min_hz=options["mel_min_hz"],
                                              mel_max_hz=options["mel_max_hz"],
                                              mel_break_freq_hertz=options["mel_break_freq_hertz"],
                                              mel_high_freq_q=options["mel_high_freq_q"])
    # reframe the data into non-overlapping 0.96s windows
    frame_transform = Frame(stft_hop_length_seconds=options["stft_hop_length_seconds"],
                            window_seconds=options["window_seconds"],
                            hop_seconds=options["window_seconds"])
    # define a preprocessing pipeline to transform audio files on load
    transforms = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # init, download data and configure data module
    data_module = EcoacousticsDataModuleSplitBySite(root=str(root_path / 'data'),
                                                    seed=seed,
                                                    sample_rate=options["sample_rate"],
                                                    segment_len=60 * options["window_seconds"],
                                                    val_prop=options["val_prop"],
                                                    test_prop=options["test_prop"],
                                                    train_batch_size=options["train_batch_size"],
                                                    eval_batch_size=options["eval_batch_size"],
                                                    num_workers=options["num_workers"],
                                                    target_attrs=list(targets.keys()),
                                                    train_transforms=transforms,
                                                    test_transforms=transforms)
    data_module.prepare_data()
    data_module.setup()
    decoder = data_module.train_data.dataset.decoder
    return data_module, decoder, frame_transform

def get_environment_mode():
    try:
        return os.environ["MODE"]
    except:
        return 'production'

def is_development():
    return get_environment_mode() == Environment.development.name

def is_production():
    return get_environment_mode() == Environment.production.name

def camel_case_to_space(label):
    return re.sub(r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r'\1', label)

def header_string(decoder: Dict,
                  sample_idx: int) -> str:
    return f"File: {decoder['fileName'][sample_idx]}"

def generate_title_string(decoder: Dict,
                          target_info: Dict,
                          targets: npt.NDArray,
                          sample_idx: int):
    title_strings = []
    for i, (target_value, (target_name, target_attr)) in enumerate(zip(targets, target_info.items())):
        if target_attr["type"] == "continuous":
            value = target_value.round(2)
        else:
            value = decoder[target_name][int(target_value)]
        attr_string = f"{target_attr['full_name']}: {value}"
        title_strings.append(attr_string)
    return header_string(decoder, sample_idx) + "\n" + ", ".join(title_strings)

def mel_spectrogram_params(sample_rate: int = 16_000,
                           stft_window_length_seconds: float = 0.025,
                           stft_hop_length_seconds: float = 0.010,
                           mel_min_hertz: float = 125.0,
                           mel_max_hertz: float = 7500.0,
                           cmap: Union[str, Colormap] = 'viridis') -> Dict[str, Any]:
    window_length = round(sample_rate * stft_window_length_seconds)
    return {"sr": sample_rate,
            "hop_length": round(sample_rate * stft_hop_length_seconds),
            "n_fft": 2 ** int(np.ceil(np.log(window_length) / np.log(2.0))),
            "y_axis": "mel",
            "x_axis": "time",
            "fmax": mel_max_hertz,
            "fmin": mel_min_hertz,
            "cmap": cmap }
