from typing import List, Tuple, Any, Union, Any, Dict

from pathlib import Path
import numpy as np
import pandas as pd
import yaml
from datetime import datetime, date, timedelta
from pytz import timezone  # type: ignore
from astral import LocationInfo  # type: ignore
from astral.sun import sun  # type: ignore

import torch  # type: ignore
from torch import nn  # type: ignore
from pytorch_lightning.trainer.supporters import CombinedLoader  # type: ignore
from ranzen.torch.data import Subset  # type: ignore
from conduit.data import TernarySample  # type: ignore
from conduit.data.structures import TrainValTestSplit  # type: ignore
from conduit.data.datamodules.audio.ecoacoustics import EcoacousticsDataModule, SoundscapeAttr  # type: ignore
from conduit.data.datasets.utils import CdtDataLoader  # type: ignore
from conduit.data.datasets.wrappers import AudioTransformer  # type: ignore
from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.types import Stage

class EcoacousticsDataModuleSplitBySite(EcoacousticsDataModule):
    def prepare_data(self,
                     *args: Any,
                     **kwargs: Any) -> None:
        # download the data
        data = Ecoacoustics(
            root=self.root,
            download=True,
            segment_len=self.segment_len,
            target_attrs=self.target_attrs,
        )
        # standardise time, normalise around sunrise
        self.__fix_time_target(data)

    def val_dataloader(self,
                       combined: bool = False,
                       **kwargs) -> CdtDataLoader:
        val_loader = self.make_dataloader(ds=self.val_data,
                                          batch_size=self.eval_batch_size,
                                          **kwargs)
        if not combined:
            return val_loader
        # construct a training loader
        train_loader = self.make_dataloader(ds=self.train_data,
                                            batch_size=self.train_batch_size,
                                            **kwargs)
        # group together into a combined loader
        loaders = { Stage.train: train_loader, Stage.validate: val_loader }
        return CombinedLoader(loaders, mode="max_size_cycle")

    def test_dataloader(self,
                        combined: bool = False,
                        **kwargs) -> Union[CdtDataLoader, CombinedLoader]:
        test_loader = self.make_dataloader(ds=self.test_data,
                                           batch_size=self.eval_batch_size,
                                           **kwargs)
        if not combined:
            return test_loader
        # construct a training dataloader
        train_loader = self.make_dataloader(ds=self.train_data,
                                            batch_size=self.train_batch_size,
                                            **kwargs)
        # group together into combined loader
        loaders = { Stage.train: train_loader, Stage.test: test_loader }
        return CombinedLoader(loaders, mode="max_size_cycle")

    def find_normalisation_stats(self) -> Dict[str, Union[float, torch.Tensor]]:
        # define the sample size as a proportion of the training dataset
        # randomly sample that many indices from within the bounds of the data
        sample_idx = np.random.randint(len(self.train_data.dataset),
                                       size=round(self.val_prop * len(self.train_data.dataset)))
        # create a subset of the data using the generated indices
        # apply the preprocess transforms on batch load
        transformed_subset = AudioTransformer(self.train_data.dataset.subset(sample_idx),
                                              transform=self.train_transforms)
        # construct a dataloader to process in batches
        subset_dataloader = CdtDataLoader(transformed_subset,
                                          batch_size=self.train_batch_size,
                                          shuffle=True,
                                          num_workers=self.num_workers)
        normaliser = NormalisationPipeline()
        # iterate through sub-sample and take the maximum value
        # across all dimensions (over the entire batch, all frames,
        # all windows and frequency bins)
        for batch_idx, batch in enumerate(subset_dataloader):
            normaliser.step(batch, batch_idx)
        # return found values
        return normaliser.to_stats()

    def _get_splits(self) -> TrainValTestSplit[Ecoacoustics]:
        # load the dataset
        all_data = Ecoacoustics(
            root=self.root,
            transform=None,
            segment_len=self.segment_len,
            target_attrs=self.target_attrs,
            sample_rate=self.sample_rate,
            download=False,
        )
        # create data subsets splitting by site
        return TrainValTestSplit(**self.split_by_site(all_data,
                                                      props={"val": self.val_prop, "test": self.test_prop},
                                                      seed=self.seed))

    def split_by_site(self,
                      data: Ecoacoustics,
                      props: Dict[str, float],
                      seed: int) -> Dict[str, Ecoacoustics]:
        """
        split observations by site, so we hold out specific sites in the validation and test sets
        we're aiming to learn a generalisable model which can predict labels in unseen recordings
        of soundscapes
        """
        props["train"] = 1 - sum(props.values())  # type: ignore
        # initialize a pseudo-random number generator
        generator = torch.Generator().manual_seed(seed)
        # extract names of unique habitats and sites
        habitats = data.metadata["habitat"].unique().tolist()
        site_names = data.metadata["site"].unique().tolist()
        # calculate the size of sections provided
        splits: Dict[str, List] = {split: [] for split, prop in props.items()}
        # for each habitat split out n sites based on props
        for habitat in habitats:
            # fetch all samples from this habitat
            habitat_samples = data.metadata.query(f"habitat == @habitat")
            # fetch all unique sites from this scoped set
            habitat_site_names = habitat_samples["site"].unique()
            # generate indices for shuffled site names using generator seed
            habitat_site_names_idx = torch.randperm(len(habitat_site_names),
                                                    generator=generator).tolist()
            # iterate over each proportion
            for split, prop in props.items():
                # calculate how many sites to separate for this split
                num_sites = round(len(habitat_site_names) * prop)
                # pop out sites from shuffled list
                split_site_idx = [habitat_site_names_idx.pop(0) for _ in range(num_sites)]
                # fetch the relevant site names
                split_site_names = habitat_site_names[split_site_idx]
                # iterate through each site
                for split_site_name in split_site_names:
                    # get all samples from that site
                    site_samples = data.metadata.query("site == @split_site_name")
                    # to address the imbalanced number of samples from sites in EC, we'll randomly
                    # remove half the samples recorded in the afternoon such that both UK and EC
                    # will have 45 for each site
                    if 'EC' in split_site_name:
                        # create a mask for the indices of samples with this site with null time
                        # (i.e. they are afternoon recordings)
                        mask = np.isnan(site_samples["time"])
                        # fetch indices of null times
                        site_sample_null_time_idx = site_samples[mask].index
                        # randomly select half of them
                        shuffle_idx = torch.randperm(len(site_sample_null_time_idx),
                                                     generator=generator).tolist()
                        # fetch true index values after shuffle
                        # TODO: halving them is tailored for a specific split proportion,
                        # we should instead do this according to the required number of samples
                        scoped_idx = round(len(shuffle_idx) / 2)
                        scoped_sample_idx = site_sample_null_time_idx[shuffle_idx][:scoped_idx]
                        site_sample_idx = site_samples[~mask].index
                        # concatenate indices together
                        reduced_idx = np.concatenate((site_sample_idx, scoped_sample_idx))
                        # fetch balanced samples
                        site_samples = data.metadata.iloc[reduced_idx]
                    # append sites
                    splits[split].extend(site_samples.index)
                # remove from the overall site name list
                for site_name in split_site_names:
                    site_names.remove(site_name)
        # create data subsets from the split indices
        return {split_name: data.subset(split_idx)
                for split_name, split_idx in splits.items()}

    def __fix_time_target(self,
                          data: Ecoacoustics):
        """
        normalize time in data stored in the metadata file
        around the time of dawn on that specific day at that co-ordinate
        """
        # load location data
        sites = self.__coordinate_metadata()
        # use location data in constructors
        for site_name, site_info in sites.items():
            # add actual timezone object using offset to sites data
            site_info["timezone"] = timezone(site_info["timezone"])
            # create an astral location for this site
            site_info["location"] = LocationInfo(site_name,
                                                 site_info["site"],
                                                 site_info["timezone"],
                                                 site_info["latitude"],
                                                 site_info["longitude"])
        # collect time values normalised to dawn at specific location
        normalised_time = np.empty(len(data.metadata))
        # fetch the sunrise time on the day in question for the specified co-ordinates
        for i, metadata in data.metadata.iterrows():
            # if this item is in the ignore index, then set its time value to null so we ignore these
            # extract the site name
            site_info = sites[metadata.habitat]
            location = site_info["location"]
            # extract the date from the filename
            date_info = metadata["fileName"][-17:-4]
            # extract year, month, day, hours, minutes and transform to integer values
            # seconds is not available
            year, month, day, hours, minutes = (int(date_info[0:4]),
                                                int(date_info[4:6]),
                                                int(date_info[6:8]),
                                                int(date_info[9:11]),
                                                int(date_info[11:13]))
            # localise the time zone to this date
            tzinfo = site_info["timezone"].localize(datetime(year, month, day)).tzinfo
            # get the data for that location
            location_data = sun(location.observer,
                                date=date(year, month, day),
                                tzinfo=tzinfo)
            # extract dawn datetime
            dawn_time = location_data["dawn"]
            # create recording datetime
            recording_time = datetime(year, month, day,
                                      hours, minutes,
                                      tzinfo=tzinfo)
            if metadata.site == 'UK2-12':
                # the clock on all KNEPP-12 recordings was off by 12 hours
                # so we make that correction
                dt = timedelta(hours=12)
                recording_time -= dt
            # if its past noon, the times were labelled incorrectly,
            # we're only going to learn off times from around dawn
            if recording_time > location_data["noon"]:
                normalised_time[i] = np.nan
            else:
                # subtract each time from sunrise time to get the diff in minutes
                normalised_time[i] = (recording_time - dawn_time).total_seconds() / 60
        # append to data frame as new column or over-write existing column
        # overwrite time to be our normalised value
        data.metadata["old_time"] = data.metadata["time"].copy()
        data.metadata["time"] = normalised_time
        # overwrite existing metadata file for when it is reloaded on setup
        data.metadata.to_csv(data._metadata_path, index=False)

    @staticmethod
    def __coordinate_metadata():
        return {
            # Forest Site, UK1 (50°55′16.763″N 0°5′23.071″E);
            "UK1": {
                "name": "Plashett Wood",
                "type": "Primary Forest",
                "site": "England",
                "timezone": "Europe/London",
                "latitude": 50.921323,
                "longitude": 0.089742,
            },
            # Secondary Forest, UK2 (50°58′8.548″N 0°22′40.646″W);
            "UK2": {
                "name": "KNEPP Estate Rewilding Area",
                "type": "Secondary Forest",
                "site": "England",
                "timezone": "Europe/London",
                "latitude": 50.969041,
                "longitude": -0.377957,
            },
            # Agricultural site, UK3 (50°52'32.4"N 0°03'33.4"W);
            "UK3": {
                "name": "Falmer",
                "type": "Agricultural Site",
                "site": "England",
                "timezone": "Europe/London",
                "latitude": 50.875654,
                "longitude": -0.059286,
            },
            # Primary Forest, EC1, (0°32′17.628″N 79°8′34.728″W);
            "EC1": {
                "name": "Reserva Tesoro Escondido",
                "type": "Primary Forest",
                "site": "Ecuador",
                "timezone": "America/Guayaquil",
                "latitude": 0.538230,
                "longitude": -79.142980,
            },
            # Secondary Forest, EC2 (0°7′12.320″N 79°16′37.103″W);
            "EC2": {
                "name": "Unknown",
                "type": "Secondary Forest",
                "site": "Ecuador",
                "timezone": "America/Guayaquil",
                "latitude": 0.120089,
                "longitude": -79.276973,
            },
            # Agricultural, EC3 (0°7′48.864″N 79°12′59.543″W);
            "EC3": {
                "name": "Unknown",
                "type": "Agricultural Site",
                "site": "Ecuador",
                "timezone": "America/Guayaquil",
                "latitude": 0.13024,
                "longitude": -79.21654,
            }
        }

class NormalisationPipeline:
    def __init__(self):
        super().__init__()
        # initialize with default values
        self.x_max: float = -np.inf
        self.x_min: float = np.inf
        # define mean and standard deviation values for each frequency bin to 0.0
        self.mean: torch.Tensor = torch.zeros(torch.Size([1, 1, 1, 1, 1, 64]))  # pylint: disable=no-member
        self.std: torch.Tensor = torch.zeros(torch.Size([1, 1, 1, 1, 1, 64]))  # pylint: disable=no-member
        # track how many observations we've seen
        self.total_observations: int = 0

    def to_stats(self) -> Dict[str, float]:
        return dict(x_max=self.x_max,
                    x_min=self.x_min,
                    mean=self.mean,
                    std=self.std) 

    def step(self,
             batch: TernarySample,
             batch_idx: int):
        """
        over all mini-batches, store the maximum and minimum values
        and track the mean and standard deviation over all data
        """
        # fetch the current number of observations in this batch
        num_observations = batch.x.size(0)
        # if this is the first batch, just cache the current values
        if not batch_idx:
            # fetch the minimum and maximum over all intensity values so far
            self.x_max = batch.x.max()
            self.x_min = batch.x.min()
            # find the mean and standard deviation along each frequency bin
            self.mean = batch.x.mean(axis=[0, 2, 4], keepdim=True)
            self.std = batch.x.std(axis=[0, 2, 4], keepdim=True)
            # store observations so far
            self.total_observations = num_observations
        # otherwise, figure out new values
        else:
            # fetch the max and min values for these observations
            current_max, current_min = batch.x.max(), batch.x.min()
            # update max and min values over all observations
            self.x_max, self.x_min = (max(self.x_max, current_max),
                                      min(self.x_min, current_min))
            # fetch the mean and std for current observations for each frequency bin
            current_mean = batch.x.mean(axis=[0, 2, 4], keepdim=True)
            current_std  = batch.x.std(axis=[0, 2, 4], keepdim=True)
            # update standard deviation over all observations
            self.std = self.update_std(self.total_observations, num_observations,
                                       self.std, current_std,
                                       self.mean, current_mean)
            # update mean over all observations
            self.mean = self.update_mean(self.total_observations, num_observations,
                                         self.mean, current_mean)
            # update number of seen observations
            self.total_observations += num_observations

    @staticmethod
    def update_std(total_observations: int,
                   num_observations: int,
                   running_std: torch.Tensor,
                   current_std: torch.Tensor,
                   running_mean: torch.Tensor,
                   current_mean: torch.Tensor) -> torch.Tensor:
        """
        calculate updated standard deviation over all batches
        """
        return torch.sqrt((total_observations / (total_observations + num_observations) * running_std**2) +  # pylint: disable=no-member
                          (num_observations / (total_observations + num_observations) * current_std**2) +
                          (total_observations * num_observations / (total_observations + num_observations)**2 * (running_mean - current_mean)**2))

    @staticmethod
    def update_mean(total_observations: int,
                    num_observations: int,
                    running_mean: torch.Tensor,
                    current_mean: torch.Tensor) -> torch.Tensor:
        """
        calculate updated mean over all batches
        """
        return ((total_observations / (total_observations + num_observations) * running_mean) +
                (num_observations / (total_observations + num_observations) * current_mean))
