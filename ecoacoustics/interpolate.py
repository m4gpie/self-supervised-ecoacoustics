from typing import Tuple

import click
import torch  # type: ignore
import wandb  # type: ignore
import librosa  # type: ignore
import numpy as np
import pandas as pd
import pytorch_lightning as pl # type: ignore

from pathlib import Path
from matplotlib import pyplot as plt
from matplotlib import gridspec as gs  # type: ignore
from librosa import display as libd

from conduit.data.datasets.audio.ecoacoustics import Ecoacoustics  # type: ignore
from ecoacoustics.utils import load_targets, load_hyperparameters
from ecoacoustics.transforms.log_mel_spectrogram import LogMelSpectrogram
from ecoacoustics.transforms.frame import Frame
from ecoacoustics.utils import get_environment_mode, is_development, mel_spectrogram_params
from ecoacoustics.callbacks.z_interpolator import ZInterpolator
from ecoacoustics.models.vae_random_forest_classifier import VAERandomForestClassifier
from ecoacoustics.models.vae_mlp_classifier import VAEMLPClassifier

# define catchment areas based on UMAP (x, y) co-ordinates
UK1_COORDS = (-5.0, -1.0, 5.0, 9.0)
UK2_COORDS = (-5.0, -1.0, 4.0, 8.0)
UK3_COORDS = (-2.0, 2.0, 11.0, 13.0)
EC1_COORDS = (-2.0, 2.0, 5.0, 8.0)
EC2_COORDS = (4.0, 7.0, 10.0, 13.0)
EC3_COORDS = (2.0, 10.0, 8.0, 13.0)
# generate groupings for sampling data points
GROUPS = [('UK1', UK1_COORDS),
          ('UK2', UK2_COORDS),
          ('UK3', UK3_COORDS),
          ('EC1', EC1_COORDS),
          ('EC2', EC2_COORDS),
          ('EC3', EC3_COORDS)]

UK_TIME_GROUPS = [('UK1', UK1_COORDS),
                  ('UK2', UK2_COORDS),
                  ('UK3', UK3_COORDS)]

def all_pairs(items):
    for i in range(len(items)):
        for j in range(i + 1, len(items)):
            yield items[i], items[j]

def load_models(root_path: Path) -> Tuple[pl.LightningModule, pl.LightningModule]:
    vae = VAERandomForestClassifier.load_from_checkpoint(root_path / 'models' / '24pe369j' / 'checkpoints' / 'model.ckpt')
    optimised_vae = VAEMLPClassifier.load_from_checkpoint(root_path / 'models' / '1e3gdoue' / 'checkpoints' / 'model.ckpt')
    return (vae, optimised_vae)

def load_target_metadata(root_path: Path,
                         target_name: str) -> Tuple[pd.DataFrame, pd.DataFrame]:
    # set target for investigation
    df1 = pd.read_csv(root_path / 'data' / 'runs' / '24pe369j' / f"{target_name}.csv")
    df2 = pd.read_csv(root_path / 'data' / 'runs' / '1e3gdoue' / f"{target_name}.csv")
    return (df1, df2)

def get_random_sample_idx_within_region(df, x_min, x_max, y_min, y_max):
    coords = np.array([df["x"], df["y"]])
    idx = np.where((x_min < coords[0, :]) &
                   (coords[0, :] < x_max) &
                   (y_min < coords[1, :]) &
                   (coords[1, :] < y_max))[0]
    values = df.iloc[idx]["s"].values
    i = np.random.randint(len(values))
    return int(values[i])

def get_interpolated_time_smoothed_frame(classifier, x1, x2, num_samples):
    # switch off gradient calculations
    with torch.no_grad():
        # encode first sample
        mean, log_variance = classifier.model.encoder(x1)
        z_flat = classifier.model.reparameterise(mean, log_variance)
        z1 = z_flat.view(x1.size(0), z_flat.size(1)).mean(axis=0)
        # and the second
        mean, log_variance = classifier.model.encoder(x2)
        z_flat = classifier.model.reparameterise(mean, log_variance)
        z2 = z_flat.view(x2.size(0), z_flat.size(1)).mean(axis=0)
        # interpolate
        interpolation = torch.as_tensor(np.linspace(z1, z2, num_samples), dtype=x1.dtype)
        decoded = classifier.model.decoder(interpolation)
        x_hats, x_log_vars = decoded.chunk(2, dim=1)
        return x_hats

def get_interpolated_single_frame(classifier, x1, x2, num_samples):
    with torch.no_grad():
        # sample the frame that has the highest activation
        idx = x1.sum(axis=[1, 2, 3]).argmax()
        # encode that particular frame
        mean, log_variance = classifier.model.encoder(x1[idx].unsqueeze(0))
        z1 = classifier.model.reparameterise(mean, log_variance)
        # do the same for x2 and take the same frame
        idx = x2.sum(axis=[1, 2, 3]).argmax()
        mean, log_variance = classifier.model.encoder(x2[idx].unsqueeze(0))
        z2 = classifier.model.reparameterise(mean, log_variance)
        # interpolate in the latent space
        interpolation = torch.as_tensor(np.linspace(z1, z2, num_samples), dtype=x1.dtype)
        decoded = classifier.model.decoder(interpolation)
        x_hats, x_log_vars = decoded.chunk(2, dim=1)
        return x_hats.exp()

def plot_interpolation(x_hats,
                       from_label: str,
                       to_label: str,
                       row: int,
                       num_rows: int,
                       fig: plt.Figure,
                       grid_spec: gs.GridSpec,
                       vmax: float = -40.0,
                       vmin: float = -60.0,
                       title: str = "",
                       cmap: str = 'plasma'):
    # using our interpolation, reconstruct x_hat based on time-averaged z
    for col, x_hat in enumerate(x_hats):
        ax = fig.add_subplot(grid_spec[row, col])
        x_hat = x_hat.squeeze(0).exp().cpu().numpy().T
        mesh = libd.specshow(librosa.amplitude_to_db(x_hat),
                             vmin=vmin,
                             vmax=vmax,
                             ax=ax,
                             **mel_spectrogram_params(cmap=cmap))
        # add a number to illustrate interpolation direction
        ax.text(0.8, 0.8, str(col + 1),  # type: ignore
                color='white',
                fontsize=8,
                transform=ax.transAxes)  # type: ignore
        if col == 0:
            ax.text(0.1, 0.1, from_label,  # type: ignore
                    color='white',
                    fontsize=8,
                    transform=ax.transAxes)  # type: ignore
        if col == len(x_hats) - 1:
            ax.text(0.1, 0.1, to_label,  # type: ignore
                    color='white',
                    fontsize=8,
                    transform=ax.transAxes)  # type: ignore
        # only add x and y axis on boundaries
        ax.set_xticklabels([])
        ax.set_xticklabels([])
        ax.set_xticks([])
        ax.set_xlabel("")
        if col == 0:
            ax.set_ylabel(title)
        if col != 0:
            ax.get_yaxis().set_visible(False)  # type: ignore
            ax.set_yticklabels([])
            ax.set_yticks([])
        if row != (num_rows - 1):
            ax.get_xaxis().set_visible(False)  # type: ignore
    # return the last axis and mesh
    return ax, mesh

@click.command()
def habitats():
    """
    interpolate between each habitat to observe changes in spectro-temporal activations
    """
    # load data hyperparams
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    # load target info
    targets, test_targets = load_targets(root_path)
    # create dataset
    data = Ecoacoustics(root=str(root_path / 'data'),
                        sample_rate=options["sample_rate"],
                        segment_len=options["window_seconds"] * 60,
                        target_attrs=targets.keys())
    # and preprocessing transforms
    spectrogram_transform = LogMelSpectrogram(sample_rate=options["sample_rate"],
                                              stft_window_length_seconds=options["stft_window_length_seconds"],
                                              stft_hop_length_seconds=options["stft_hop_length_seconds"],
                                              log_offset=options["log_offset"],
                                              num_mel_bins=options["num_mel_bins"],
                                              mel_min_hz=options["mel_min_hz"],
                                              mel_max_hz=options["mel_max_hz"],
                                              mel_break_freq_hertz=options["mel_break_freq_hertz"],
                                              mel_high_freq_q=options["mel_high_freq_q"])
    # reframe the data into non-overlapping 0.96s windows
    frame_transform = Frame(stft_hop_length_seconds=options["stft_hop_length_seconds"],
                            window_seconds=options["window_seconds"],
                            hop_seconds=options["window_seconds"])
    # define a preprocessing pipeline to transform audio files on load
    transforms = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # load checkpoint
    vae, optimised_vae = load_models(root_path)
    # load the metadata for the habitat target
    _, df = load_target_metadata(root_path, "habitat")
    _, label_dict = data._label_encode(data.metadata[data.target_attrs])
    # invert so we can fetch by string, not encoding
    label_dict = { v: k for (k, v) in label_dict["habitat"].items() }
    # fetch a random sample from each grouping
    habitat_samples = {}
    for label, coords in GROUPS:
        habitat_df = df[df["target"] == label_dict[label]]
        sample_idx = get_random_sample_idx_within_region(habitat_df, *coords)
        x = transforms(data[sample_idx].x).squeeze(0)
        habitat_samples[label] = x
    # generate 3 separate figures according to UK, EC and between
    uk_pairs = [('UK1', 'EC1'),
                ('UK1', 'UK2'),
                ('EC1', 'EC2'),
                ('UK1', 'UK3'),
                ('EC1', 'EC3'),
                ('UK2', 'UK3'),
                ('EC2', 'EC3')]
    # iterate over all groups
    for groups in [uk_pairs]:
        # generate a figure and grid spec for this group
        num_frames = 8
        num_rows = len(groups)
        fig = plt.figure(figsize=(11.69, 8.27), dpi=80)
        outer_grid = gs.GridSpec(2, 3,
                                 width_ratios=[0.01, 1.0, 0.03],
                                 height_ratios=[1.0, 0.01])
        inner_grid = gs.GridSpecFromSubplotSpec(num_rows, num_frames,
                                                subplot_spec=outer_grid[1],
                                                wspace=0.0)
        # step through in twos to increment row correctly
        for i, row in enumerate(groups):
            from_label, to_label = groups[i]
            x1 = habitat_samples[from_label]
            x2 = habitat_samples[to_label]
            x_hat_1 = get_interpolated_time_smoothed_frame(vae, x1, x2, num_frames)
            x_hat_2 = get_interpolated_time_smoothed_frame(optimised_vae, x1, x2, num_frames)
            ax, mesh = plot_interpolation(x_hats=x_hat_2,
                                          from_label=from_label,
                                          to_label=to_label,
                                          vmax=-45.0,
                                          # vmin=vmin,
                                          row=i,
                                          num_rows=num_rows,
                                          fig=fig,
                                          grid_spec=inner_grid,
                                          cmap='viridis')
        plt.colorbar(mesh,
                     cax=fig.add_subplot(outer_grid[:-1, -1]),
                     format='%+3.1f dB')   # type: ignore
        # add single axes labels outside the grid
        x_label_ax = fig.add_subplot(outer_grid[1, 1])
        x_label_ax.set_xlabel('Time (s)')
        x_label_ax.set_frame_on(False)
        x_label_ax.set_xticks([])
        x_label_ax.set_xticks([])
        x_label_ax.set_yticks([])
        y_label_ax = fig.add_subplot(outer_grid[:, 0])
        y_label_ax.set_ylabel('Frequency (Hz)')
        y_label_ax.set_frame_on(False)
        y_label_ax.set_xticks([])
        y_label_ax.set_yticks([])
        habitat_names = ", ".join(np.unique(groups).tolist())
        fig.suptitle(f"Interpolating between recordings from {habitat_names}\n"
                     f"using time-averaged acoustic embeddings")
        habitat_names = "-".join(np.unique(groups).tolist())
        fig.savefig(root_path / 'docs' / 'assets' / f'{habitat_names}-habitat-interpolation.png')
    plt.show()

@click.command()
def times():
    """
    for each habitat cluster, select a data point from each of before,
    around and after dawn. render interpolation in a single plot for each habitat
    where each row corresponds to before/after/around dawn,
    interpolating from before to dawn and after to dawn, and before to after dawn
    """
    # load data hyperparams
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    # load target info
    targets, test_targets = load_targets(root_path)
    # create dataset
    data = Ecoacoustics(root=str(root_path / 'data'),
                        sample_rate=options["sample_rate"],
                        segment_len=options["window_seconds"] * 60,
                        target_attrs=targets.keys())
    # and preprocessing transforms
    spectrogram_transform = LogMelSpectrogram(sample_rate=options["sample_rate"],
                                              stft_window_length_seconds=options["stft_window_length_seconds"],
                                              stft_hop_length_seconds=options["stft_hop_length_seconds"],
                                              log_offset=options["log_offset"],
                                              num_mel_bins=options["num_mel_bins"],
                                              mel_min_hz=options["mel_min_hz"],
                                              mel_max_hz=options["mel_max_hz"],
                                              mel_break_freq_hertz=options["mel_break_freq_hertz"],
                                              mel_high_freq_q=options["mel_high_freq_q"])
    # reframe the data into non-overlapping 0.96s windows
    frame_transform = Frame(stft_hop_length_seconds=options["stft_hop_length_seconds"],
                            window_seconds=options["window_seconds"],
                            hop_seconds=options["window_seconds"])
    # define a preprocessing pipeline to transform audio files on load
    transforms = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # load checkpoint
    vae, optimised_vae = load_models(root_path)
    # load the metadata for the time target
    _, df = load_target_metadata(root_path, "time")
    # set index to use original
    df.set_index("s")
    # -------------------------------- UK ---------------------------------- #
    # fetch 3 random samples from each habitat
    # one before, one during and one after the dawn chorus
    # generate a figure and grid spec
    num_frames = 8
    num_rows = 9
    fig = plt.figure(figsize=(11.69, 8.27), dpi=80)
    outer_grid = gs.GridSpec(2, 3,
                             width_ratios=[0.01, 1.0, 0.03],
                             height_ratios=[1.0, 0.01])
    inner_grid = gs.GridSpecFromSubplotSpec(num_rows, num_frames,
                                            subplot_spec=outer_grid[1],
                                            wspace=0.0)
    for group_row_i, (label, coords) in zip(range(0, 9, 3), UK_TIME_GROUPS):
        # extract observations from this habitat
        habitats = data.metadata["habitat"][df.index]
        label_habitats = habitats.where(habitats == label)
        label_habitats_idx = label_habitats[~label_habitats.isnull()].index.to_numpy()
        habitat_df = df.loc[label_habitats_idx]
        # extract samples from before, around and after dawn
        before_dawn_df = habitat_df[habitat_df["target"] < -20]
        around_dawn_df = habitat_df[(-20 <= habitat_df["target"]) & (habitat_df["target"] < 70)]
        after_dawn_df = habitat_df[70 <= habitat_df["target"]]
        # select sample from within region
        before_dawn_sample_idx = get_random_sample_idx_within_region(before_dawn_df, *coords)
        around_dawn_sample_idx = get_random_sample_idx_within_region(around_dawn_df, *coords)
        after_dawn_sample_idx = get_random_sample_idx_within_region(after_dawn_df, *coords)
        before_dawn_x = transforms(data[before_dawn_sample_idx].x).squeeze(0)
        around_dawn_x = transforms(data[around_dawn_sample_idx].x).squeeze(0)
        after_dawn_x = transforms(data[after_dawn_sample_idx].x).squeeze(0)
        # define time labels
        labels = ['t < -20', '-20 <= t < 70', '70 <= t']
        group = [(labels[0], before_dawn_x),
                 (labels[1], around_dawn_x),
                 (labels[2], after_dawn_x)]
        for i, ((from_label, x1), (to_label, x2)) in enumerate(all_pairs(group)):
            x_hat_1 = get_interpolated_time_smoothed_frame(vae, x1, x2, num_frames)
            x_hat_2 = get_interpolated_time_smoothed_frame(optimised_vae, x1, x2, num_frames)
            ax, mesh = plot_interpolation(x_hats=x_hat_2,
                                          from_label=from_label,
                                          to_label=to_label,
                                          row=group_row_i + i,
                                          num_rows=num_rows,
                                          title=label,
                                          fig=fig,
                                          grid_spec=inner_grid,
                                          cmap='magma')
    plt.colorbar(mesh,
                 cax=fig.add_subplot(outer_grid[:-1, -1]),
                 format='%+3.1f dB')   # type: ignore
    # add single axes labels outside the grid
    x_label_ax = fig.add_subplot(outer_grid[1, 1])
    x_label_ax.set_xlabel('Time (s)')
    x_label_ax.set_frame_on(False)
    x_label_ax.set_xticks([])
    x_label_ax.set_yticks([])
    y_label_ax = fig.add_subplot(outer_grid[:, 0])
    y_label_ax.set_ylabel('Frequency (Hz)')
    y_label_ax.set_frame_on(False)
    y_label_ax.set_xticks([])
    y_label_ax.set_yticks([])
    fig.suptitle(f"Interpolating between recordings from\n"
                 f"before, around and after dawn in the UK\n"
                 f"using time-averaged acoustic embeddings")
    fig.savefig(root_path / 'docs' / 'assets' / f'around-dawn-interpolation-UK.png')
    plt.show()


@click.command()
def species_distributions():
    """
    for each habitat, pick the observation with the minimum and maximum
    species abundance / richness measures to interpolate between
    """
    # load data hyperparams
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    # load target info
    targets, test_targets = load_targets(root_path)
    # create dataset
    data = Ecoacoustics(root=str(root_path / 'data'),
                        sample_rate=options["sample_rate"],
                        segment_len=options["window_seconds"] * 60,
                        target_attrs=targets.keys())
    # and preprocessing transforms
    spectrogram_transform = LogMelSpectrogram(sample_rate=options["sample_rate"],
                                              stft_window_length_seconds=options["stft_window_length_seconds"],
                                              stft_hop_length_seconds=options["stft_hop_length_seconds"],
                                              log_offset=options["log_offset"],
                                              num_mel_bins=options["num_mel_bins"],
                                              mel_min_hz=options["mel_min_hz"],
                                              mel_max_hz=options["mel_max_hz"],
                                              mel_break_freq_hertz=options["mel_break_freq_hertz"],
                                              mel_high_freq_q=options["mel_high_freq_q"])
    # reframe the data into non-overlapping 0.96s windows
    frame_transform = Frame(stft_hop_length_seconds=options["stft_hop_length_seconds"],
                            window_seconds=options["window_seconds"],
                            hop_seconds=options["window_seconds"])
    # define a preprocessing pipeline to transform audio files on load
    transforms = torch.nn.Sequential(spectrogram_transform, frame_transform)
    # load checkpoint
    vae, optimised_vae = load_models(root_path)
    # load the metadata for the habitat target
    for metric_name, metric in [("richness", "NN"), ("abundance", "N0")]:
        _, df = load_target_metadata(root_path, metric)
        # set the dataframe to use true sample indices
        df = df.set_index("s")
        num_frames = 8
        num_rows = 6
        fig = plt.figure(figsize=(11.69, 8.27), dpi=80)
        outer_grid = gs.GridSpec(2, 3,
                                 width_ratios=[0.01, 1.0, 0.03],
                                 height_ratios=[1.0, 0.01])
        inner_grid = gs.GridSpecFromSubplotSpec(num_rows, num_frames,
                                                subplot_spec=outer_grid[1],
                                                wspace=0.0)
        for i, (label, coords) in enumerate(GROUPS):
            # extract observations from our species and coordinate data for each habitat
            habitats = data.metadata["habitat"][df.index]
            label_habitats = habitats.where(habitats == label)
            label_habitats_idx = label_habitats[~label_habitats.isnull()].index.to_numpy()
            habitat_df = df.loc[label_habitats_idx]
            x, y = np.array([habitat_df["x"], habitat_df["y"]])
            # locate all observations from this habitat in this particular co-ordinate region
            x_min, x_max, y_min, y_max = coords
            idx = np.where((x_min < x) &
                           (x < x_max) &
                           (y_min < y) &
                           (y < y_max))[0]
            umap_region_habitat_df = habitat_df.iloc[idx]
            # select those with the highest and lowest species count
            max_idx = umap_region_habitat_df["target"].argmax()
            min_idx = umap_region_habitat_df["target"].argmin()
            species_count_1 = int(umap_region_habitat_df["target"].max())
            species_count_2 = int(umap_region_habitat_df["target"].min())
            true_min_max_idx = umap_region_habitat_df.iloc[[max_idx, min_idx]].index
            # extract waveform and transform into spectrograms
            x1 = transforms(data[umap_region_habitat_df.iloc[[max_idx, min_idx]].index.tolist()[0]].x).squeeze(0)
            x2 = transforms(data[umap_region_habitat_df.iloc[[max_idx, min_idx]].index.tolist()[1]].x).squeeze(0)
            x_hat_1 = get_interpolated_time_smoothed_frame(vae, x1, x2, num_frames)
            x_hat_2 = get_interpolated_time_smoothed_frame(optimised_vae, x1, x2, num_frames)
            from_label = f"{label}: {species_count_1}"
            to_label = f"{label}: {species_count_2}"
            ax, mesh = plot_interpolation(x_hats=x_hat_2,
                                          from_label=from_label,
                                          to_label=to_label,
                                          row=i,
                                          num_rows=num_rows,
                                          fig=fig,
                                          grid_spec=inner_grid,
                                          cmap='inferno')
        plt.colorbar(mesh,
                     cax=fig.add_subplot(outer_grid[:-1, -1]),
                     format='%+3.1f dB')   # type: ignore
        # add single axes labels outside the grid
        x_label_ax = fig.add_subplot(outer_grid[1, 1])
        x_label_ax.set_xlabel('Time (s)')
        x_label_ax.set_frame_on(False)
        x_label_ax.set_xticks([])
        x_label_ax.set_yticks([])
        y_label_ax = fig.add_subplot(outer_grid[:, 0])
        y_label_ax.set_ylabel('Frequency (Hz)')
        y_label_ax.set_frame_on(False)
        y_label_ax.set_xticks([])
        y_label_ax.set_yticks([])
        fig.suptitle(f"Interpolating between recorder sites with minimum and\n"
                     f"maximum species {metric_name} across habitat gradients\n"
                     f"using time-averaged acoustic embeddings")
        fig.savefig(root_path / 'docs' / 'assets' / f'species-{metric_name}-interpolation.png')
    plt.show()


@click.group()
def interpolate():
    pass

interpolate.add_command(habitats)
interpolate.add_command(times)
interpolate.add_command(species_distributions)

if __name__ == '__main__':
    interpolate()
