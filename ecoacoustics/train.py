import click
import wandb  # type: ignore
import pytorch_lightning as pl # type: ignore
from pathlib import Path

from ecoacoustics.utils import load_targets, load_hyperparameters, preprocess, mel_spectrogram_params
from ecoacoustics.utils import get_environment_mode, is_development
from ecoacoustics.callbacks.z_umap import ZUMAP
from ecoacoustics.callbacks.reconstruct import Reconstructor
from ecoacoustics.callbacks.random_forest import RandomForest
from ecoacoustics.callbacks.mlp_metrics import MLPMetrics
from ecoacoustics.models.vae_random_forest_classifier import VAERandomForestClassifier
from ecoacoustics.models.vae_mlp_classifier import VAEMLPClassifier

@click.command()
@click.option('--seed', type=int, default=42, help="random seed")
def vae_random_forest_classifier(seed):
    """
    train a VAE on ecoacoustic dataset and use time-averaged sampled
    latent vector z in a random forest to estimate target variables
    """
    pl.seed_everything(seed)
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    options.update({"seed": seed })
    targets, test_targets = load_targets(root_path)
    data_module, label_decoder, frame_transform = preprocess(root_path, seed, targets, options)
    # define our model, if we're doing a deterministic run, we use just an autoencoder
    model = VAERandomForestClassifier(learning_rate=options["learning_rate"],
                                      vggish_pretrain=options["vggish_pretrain"],
                                      vggish_frozen=options["vggish_frozen"],
                                      latent_features=options["latent_features"],
                                      gamma=options["gamma"],
                                      scheduler_step_size=options["scheduler_step_size"],
                                      scheduler_initial_step=options["scheduler_initial_step"])
    # compute a random forest on the latent space for validation
    random_forest = RandomForest(targets=test_targets,
                                 decoder=label_decoder,
                                 model_name='VAE',
                                 num_estimators=options["num_estimators"])
    # upload reconstructions
    specgram_params = mel_spectrogram_params(sample_rate=options["sample_rate"],
                                             stft_window_length_seconds=options["stft_window_length_seconds"],
                                             stft_hop_length_seconds=options["stft_hop_length_seconds"],
                                             mel_min_hertz=options["mel_min_hz"],
                                             mel_max_hertz=options["mel_max_hz"],
                                             cmap='viridis')
    reconstruct = Reconstructor(targets=targets,
                                frame_transform=frame_transform,
                                decoder=label_decoder,
                                reconstruct_every_n_steps=options["reconstruct_every_n_steps"],
                                specgram_params=specgram_params)
    # stop when f1 score is highest
    early_stop = pl.callbacks.EarlyStopping(monitor="val/f1_score", mode="max", patience=options["patience"])
    # checkpoint when f1 score is highest
    checkpoint = pl.callbacks.ModelCheckpoint(monitor="val/f1_score", mode="max")
    # setup trainer options
    env = get_environment_mode()
    train_options = options[env]
    # update the trainer parameters
    train_options.update({"accelerator": "auto",
                          "devices": "auto",
                          "callbacks": [early_stop, checkpoint, reconstruct, random_forest],
                          "logger": pl.loggers.WandbLogger(project="variational-autoencoder-ecoacoustics"),
                          "fast_dev_run": is_development() })
    # init the trainer
    trainer = pl.Trainer(**train_options)
    # update remote config with all hyperparameters
    wandb.config.update(options)
    # train the model
    trainer.fit(model,
                data_module.train_dataloader(shuffle=True),
                data_module.val_dataloader(combined=True, shuffle=True))
    # if we've saved a checkpoint
    if checkpoint.best_model_path:
        # upload best model to W&B
        wandb.save(checkpoint.best_model_path)

@click.command()
@click.option('--seed', type=int, default=42, help="random seed")
@click.option('--checkpoint-path', '-m', required=True, type=str, help="path to pytorch checkpoint for trained VAE")
def vae_mlp_classifier(seed,
                       checkpoint_path):
    """
    train a VAE on ecoacoustic dataset and use time-averaged
    sampled latent vector z in an MLP to predict target variables
    """
    pl.seed_everything(seed)
    root_path = Path().resolve()
    options = load_hyperparameters(root_path)
    options.update({"seed": seed })
    targets, test_targets = load_targets(root_path)
    data_module, label_decoder, frame_transform = preprocess(root_path, seed, targets, options)
    # load the checkpoint
    print(f"Using checkpoint {checkpoint_path}")
    # define our model
    model = VAEMLPClassifier(checkpoint_path=checkpoint_path,
                             learning_rate=options["learning_rate"],
                             targets=test_targets)
    # define callbacks
    # generate MLP metrics
    mlp_metrics = MLPMetrics(targets=test_targets,
                             decoder=label_decoder)
    # stop when f1 score is highest
    early_stop = pl.callbacks.EarlyStopping(monitor="val/f1_score",
                                            mode="max",
                                            patience=options["patience"])
    # checkpoint when f1 score is highest
    checkpoint = pl.callbacks.ModelCheckpoint(monitor="val/f1_score",
                                              mode="max")
    # upload reconstructions
    specgram_params = mel_spectrogram_params(sample_rate=options["sample_rate"],
                                             stft_window_length_seconds=options["stft_window_length_seconds"],
                                             stft_hop_length_seconds=options["stft_hop_length_seconds"],
                                             mel_min_hertz=options["mel_min_hz"],
                                             mel_max_hertz=options["mel_max_hz"],
                                             cmap='viridis')
    # upload reconstructions
    reconstruct = Reconstructor(targets=targets,
                                frame_transform=frame_transform,
                                decoder=label_decoder,
                                reconstruct_every_n_steps=options["reconstruct_every_n_steps"],
                                specgram_params=specgram_params)
    # setup trainer options
    env = get_environment_mode()
    train_options = options[env]
    # update the trainer parameters
    train_options.update({"accelerator": "auto",
                          "devices": "auto",
                          "callbacks":[early_stop, checkpoint, reconstruct, mlp_metrics],
                          "logger": pl.loggers.WandbLogger(project="variational-autoencoder-ecoacoustics"),
                          "fast_dev_run": is_development() })
    # setup a trainer
    trainer = pl.Trainer(**train_options)
    # update remote config with all hyperparameters
    wandb.config.update(options)
    # train the model
    trainer.fit(model,
                data_module.train_dataloader(shuffle=True),
                data_module.val_dataloader(combined=False, shuffle=True))
    # if we've saved a checkpoint
    if checkpoint.best_model_path:
        # upload best model to W&B
        wandb.save(checkpoint.best_model_path)

@click.group()
def train():
    pass

train.add_command(vae_random_forest_classifier)
train.add_command(vae_mlp_classifier)

if __name__ == '__main__':
    train()
