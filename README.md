# Improving interpretability of machine-learned ecoacoustic feature embeddings using variational inference

## Overview

This project was completed for my MSc Intelligent and Adaptive Systems at the University of Sussex and builds on the latest research using machine-learning to generate ecoacoustic feature embeddings. We deploy a Variational Auto-encoder architecture to train a model to reconstruct 60x96x64 log mel spectrograms per audio recording. Pre-trained VGGish is used to seed the weights of the VAE's encoder before training to make the model more comparable with S. Sethi's work.

Results output include training metrics, reconstructed spectrograms, uniform manifold approximation and projection (UMAP) reductions of acoustic embeddings, and interpolation in the latent space between specific data points drawn from the test set.

The model is trained and evaluated on an ecoacoustic dataset collected across a gradient of 3 habitat types with a total of 6 different habitats, 3 from the UK and 3 from Ecuador.

A change log / development diary is available for the project [here](./docs/CHANGELOG.md), and an initial project outline as a Gantt chart.

## Setup
The model was developed using Python 3.8.11 with Poetry used as a package manager. Ensure you have the correct version of Python installed, then to setup the project perform the following steps:

```
# setup a virtual environment
python -m venv env
source ./env/bin/activate

# upgrade pip
pip install --upgrade pip

# install all dependencies
poetry install

# track remote statistics on wandb, setup an account and login through the terminal
wandb login

# execute the cli to display commands
python main.py
```

## Training

```
# train the VAE, using a secondary random forest on vectors sampled from the latent space to validate classification accuracy
MODE=production python main.py train vae-random-forest-classifier

# retrain the VAE with an optimised encoder using a MLP as the secondary classifier
MODE=production python main.py train vae-mlp-classifier -m <path-to-model-checkpoint>
```

## Testing

Models are available for download from [OneDrive](https://universityofsussex-my.sharepoint.com/:f:/g/personal/kag25_sussex_ac_uk/EpHO2FZHO1xPr-WSSzIfbegBxAR9YdujXwfPwIu2WAUK2g?e=QOPL16). Download them to the `./models` folder.

```
# test the VGGish's accuracy on the dataset
MODE=production python main.py test vggish-random-forest-classifier

# test the accuracy of VAE generated embeddings on the test set using a random forest
MODE=production python main.py test vae-random-forest-classifier

# test the accuracy of optimised VAE generated embeddings on the test set using the trained MLP
MODE=production python main.py test vae-mlp-classifier -m <path-to-optimised-model-checkpoint>
```

## Interpolation

Once models are downloaded using the same fixed paths, interpolation in the latent space can be performed using the following commands:

```
# interpolate between habitats
python main.py interpolate habitats

# interpolate between before/during/after dawn in selected habitats
python main.py interpolate times

# interpolate between species richness metrics in selected habitats
python main.py interpolate species-distributions
```

## Contributions
Many thanks to Ivor Simpson and Alice Eldridge for supervising the project and providing fantastic support throughout the research. Additional thanks to Oliver Thomas and Myles Bartlett for building the data loading pipeline [`conduit`]() and assisting with debugging the architecture and testing the model. Thanks to Rory Gibb for proof reading the final report and Hanna Tolle for sharing in the joy and pain of pursuing a challenging research project.

## Troubleshooting
`conduit` is in active development and the project runs off a particular branch with an open pull request. If you are running an old version of conduit by accident, you can do the following to update to the latest version specified in `pyproject.toml`:

```
poetry lock --no-update
pip uninstall conduit
poetry install --remove-untracked
```
