import click

from ecoacoustics.train import train
from ecoacoustics.test import test
from ecoacoustics.interpolate import interpolate

@click.group()
def cli():
    pass

cli.add_command(train)
cli.add_command(test)
cli.add_command(interpolate)

if __name__ == '__main__':
    cli()
